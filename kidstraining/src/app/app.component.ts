import { Component } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

import { DataService } from './services/dataService.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'kidstraining';
  // @Output() checkLayout: EventEmitter<string> = new EventEmitter();
  lang: string;
  layout: string;
  userName;
  constructor(
    private router: Router,
    public translate: TranslateService,
    public service: DataService
  ) {
    this.userName = localStorage.getItem('userName') || '';

    translate.addLangs(['ar', 'en']);
    let defaultLang = 'en';
    localStorage.getItem('lang') == undefined
      ? localStorage.setItem('lang', defaultLang)
      : (defaultLang = localStorage.getItem('lang'));
    translate.setDefaultLang(defaultLang);
    this.translate.currentLang = localStorage.getItem('lang');

    if (defaultLang == 'ar') {
      document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');
      document.getElementsByTagName('html')[0].setAttribute('lang', 'ar');
    } else {
      // document.getElementsByTagName('html')[0].removeAttribute('dir');
      document.getElementsByTagName('html')[0].setAttribute('dir', 'ltr');
      document.getElementsByTagName('html')[0].setAttribute('lang', 'en');
    }
  }

  getLayout(data) {
    this.lang = data;
    this.layout = data == 'EN' ? 'ltr' : 'rtl';
    // this.checkLayout.emit(this.lang);
  }

  goTop() {
    window.scrollTo(0, 0);
  }
}
