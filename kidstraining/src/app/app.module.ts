import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpClient,
} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgxDropzoneModule } from 'ngx-dropzone';

import { AppRoutingModule } from './app-routing.module';
import { Ng2SmartTableModule, LocalDataSource } from 'ng2-smart-table';
import { Select2Module } from 'ng-select2-component';
import { FormsModule, FormGroup, ReactiveFormsModule } from '@angular/forms';
import { NgxSpinnerModule } from 'ngx-spinner';
import { DatePipe } from '@angular/common';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import { SigninComponent } from './components/modules/signin/signin.component';
import { RegisterComponent } from './components/modules/register/register.component';
import { HomeSliderComponent } from './components/sharedComponents/home-slider/home-slider.component';
import { HomeComponent } from './components/sharedComponents/home/home.component';
import { AboutUsComponent } from './components/sharedComponents/about-us/about-us.component';
import { ContactUsComponent } from './components/sharedComponents/contact-us/contact-us.component';
import { NavbarComponent } from './components/sharedComponents/navbar/navbar.component';
import { FooterComponent } from './components/sharedComponents/footer/footer.component';
import { NotFoundComponent } from './components/sharedComponents/not-found/not-found.component';
import { ClassesComponent } from './components/sharedComponents/classes/classes.component';
import { FacilityComponent } from './components/sharedComponents/facility/facility.component';
import { PopularTeachersComponent } from './components/sharedComponents/popular-teachers/popular-teachers.component';
import { BecomeTeacherComponent } from './components/sharedComponents/become-teacher/become-teacher.component';
import { AppointmentComponent } from './components/sharedComponents/appointment/appointment.component';
import { TestimonialComponent } from './components/sharedComponents/testimonial/testimonial.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { UserProfileComponent } from './components/modules/user-profile/user-profile.component';
import { EditProfileComponent } from './components/modules/user-profile/edit-profile/edit-profile.component';
import { PaymentAndPlanComponent } from './components/modules/payment-and-plan/payment-and-plan.component';
import { ChildrenComponent } from './components/modules/children/children.component';
import { AddEditChildrenComponent } from './components/modules/children/add-edit-children/add-edit-children.component';
import { ChildExamComponent } from './components/modules/child-exam/child-exam.component';
import { ChildProgramComponent } from './components/modules/child-program/child-program.component';
import { ChildReportComponent } from './components/modules/child-report/child-report.component';
import { Httpconfig } from './services/httpconfig.interceptor.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { DeleteActionComponent } from './components/sharedComponents/gridColumns/delete-action/delete-action.component';
import { ChangeStatusActionComponent } from './components/sharedComponents/gridColumns/change-status-action/change-status-action.component';
import { ImageViewComponent } from './components/sharedComponents/gridColumns/image-view/image-view.component';
import { EditDeleteActionsComponent } from './components/sharedComponents/gridColumns/edit-delete-actions/edit-delete-actions.component';
import { NgxIntlTelInputModule } from 'ngx-intl-tel-input';
import { OtpPopupComponent } from './components/modules/otp-popup/otp-popup.component';
import { RenewPlanPopupComponent } from './components/modules/renew-plan-popup/renew-plan-popup.component';
import { ChildTestComponent } from './components/modules/children/child-test/child-test.component';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
@NgModule({
  declarations: [
    AppComponent,
    DeleteActionComponent,
    ChangeStatusActionComponent,
    ImageViewComponent,
    EditDeleteActionsComponent,
    SigninComponent,
    RegisterComponent,
    HomeSliderComponent,
    HomeComponent,
    AboutUsComponent,
    ContactUsComponent,
    NavbarComponent,
    FooterComponent,
    NotFoundComponent,
    ClassesComponent,
    FacilityComponent,
    PopularTeachersComponent,
    BecomeTeacherComponent,
    AppointmentComponent,
    TestimonialComponent,
    UserProfileComponent,
    EditProfileComponent,
    PaymentAndPlanComponent,
    ChildrenComponent,
    AddEditChildrenComponent,
    ChildExamComponent,
    ChildProgramComponent,
    ChildReportComponent,
    OtpPopupComponent,
    RenewPlanPopupComponent,
    ChildTestComponent,
  ],
  imports: [
    NgxIntlTelInputModule,
    Ng2SmartTableModule,
    Select2Module,
    NgxDropzoneModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    FormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgbModule,
    TooltipModule.forRoot(),
    ModalModule.forRoot(),
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
    }),
  ],
  providers: [
    DatePipe,
    { provide: HTTP_INTERCEPTORS, useClass: Httpconfig, multi: true },
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
