import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditChildrenComponent } from './components/modules/children/add-edit-children/add-edit-children.component';
import { ChildTestComponent } from './components/modules/children/child-test/child-test.component';
import { RegisterComponent } from './components/modules/register/register.component';
import { SigninComponent } from './components/modules/signin/signin.component';
import { UserProfileComponent } from './components/modules/user-profile/user-profile.component';
import { AboutUsComponent } from './components/sharedComponents/about-us/about-us.component';
import { AppointmentComponent } from './components/sharedComponents/appointment/appointment.component';
import { BecomeTeacherComponent } from './components/sharedComponents/become-teacher/become-teacher.component';
import { ClassesComponent } from './components/sharedComponents/classes/classes.component';
import { ContactUsComponent } from './components/sharedComponents/contact-us/contact-us.component';
import { FacilityComponent } from './components/sharedComponents/facility/facility.component';
import { HomeComponent } from './components/sharedComponents/home/home.component';
import { NotFoundComponent } from './components/sharedComponents/not-found/not-found.component';
import { PopularTeachersComponent } from './components/sharedComponents/popular-teachers/popular-teachers.component';
import { TestimonialComponent } from './components/sharedComponents/testimonial/testimonial.component';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutUsComponent },
  { path: 'contact', component: ContactUsComponent },
  { path: 'classes', component: ClassesComponent },
  { path: 'facility', component: FacilityComponent },
  { path: 'popular-teachers', component: PopularTeachersComponent },
  { path: 'become-teacher', component: BecomeTeacherComponent },
  { path: 'appointment', component: AppointmentComponent },
  { path: 'testimonial', component: TestimonialComponent },
  { path: 'login', component: SigninComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'myProfile', component: UserProfileComponent },
  { path: 'AddEditChild/:id', component: AddEditChildrenComponent },
  { path: 'childtest/:id', component: ChildTestComponent },
  { path: '**', component: NotFoundComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' }),
  ],
  exports: [RouterModule],
})
export class AppRoutingModule {}
