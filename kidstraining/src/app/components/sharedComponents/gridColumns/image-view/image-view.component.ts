import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ViewCell } from 'ng2-smart-table';
import { environment } from '../../../../../environments/environment';
import { DataService } from '../../../../services/dataService.service';
@Component({
  selector: 'app-image-view',
  templateUrl: './image-view.component.html',
  styles: [
  ]
})
export class ImageViewComponent implements ViewCell,OnInit {

  renderValue: string;
  baseImage = environment.apiUrl
  @Input() value;
  @Input() rowData: any;

  @Output() save: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    this.renderValue = this.value;
    console.log(this.renderValue)
  }

  onClick(value) {
    console.log(this.rowData);
    this.save.emit(value.target.value);

  }
  eventCheck(event) {
    console.log(event);
  }

}
