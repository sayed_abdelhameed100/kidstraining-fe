import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ViewCell } from 'ng2-smart-table';
import { DataService } from '../../../../services/dataService.service';


@Component({
  selector: 'app-change-status-action',
  templateUrl: './change-status-action.component.html',
  styles: [
  ]
})
export class ChangeStatusActionComponent implements ViewCell,OnInit {

  renderValue: boolean;

  @Input() value;
  @Input() rowData: any;
  @Input() apiCall: string;
  @Output() save: EventEmitter<any> = new EventEmitter();

  constructor(public service: DataService) {

  }

  ngOnInit() {
    this.renderValue = this.value;
  }

  onClick(value) {
    console.log(this.rowData);
    this.save.emit(value.target.value);
    this.service.get(this.apiCall + '/ChangeStatus?Id=' + this.rowData.id).subscribe(
      result => {
        this.service.confirmBox();
      }
    )
  }
  eventCheck(event) {
    console.log(event);
  }

}
