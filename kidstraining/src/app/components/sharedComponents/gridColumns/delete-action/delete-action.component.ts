import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ViewCell } from 'ng2-smart-table';
import Swal from 'sweetalert2';
import { DataService } from '../../../../services/dataService.service';
@Component({
  selector: 'app-delete-action',
  templateUrl: './delete-action.component.html',
  styles: [
  ]
})
export class DeleteActionComponent implements ViewCell, OnInit {

  @Input() value;
  @Input() rowData: any;

  @Input() navegateURL: string;
  @Input() icon: string;
  @Input() param: string;
  @Input() authCode: string;
  @Input() apiCall: string;
  renderValue;
  iconClass: string = "";
  parameter: string = "";
  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, public service: DataService, public translate: TranslateService) {

  }


  ngOnInit() {
    this.renderValue = this.value;
    this.iconClass = this.icon;
    this.parameter = this.rowData[this.param];
  }

  onClick(value) {

    if (this.authCode == '0') {
     // this.service.checkPermissionsIsAdmin() ? null : this.router.navigate(['./PageNotAuthorized']);
     // if (this.service.checkPermissionsIsAdmin()) {
        this.deleteRows();
      //}
    }

    else {
      //if (this.service.checkAuths(this.authCode)) {
        this.deleteRows();
     // }
    }

  }

  deleteRows = () => {
    Swal.fire({
      title: this.translate.instant('deleteConfirm'),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: this.translate.instant('yes'),
      cancelButtonText: this.translate.instant('no')
    }).then((result: any) => {
      if (result.value) {
        Swal.fire(
          this.translate.instant('remove'),
          this.translate.instant('removeDone'),
          'success'
        )
        this.confirmDelete();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          this.translate.instant('cancel'),
          this.translate.instant('cancelRemove'),
          'error'
        )
      }
    })
  }

  confirmDelete = () => {
    let ids = [];
    ids.push(this.rowData.id)
    this.service.post(this.apiCall + '/Delete', ids).subscribe(
      result => {
        this.save.emit(this.rowData.id);
      }
    )
  }

}
