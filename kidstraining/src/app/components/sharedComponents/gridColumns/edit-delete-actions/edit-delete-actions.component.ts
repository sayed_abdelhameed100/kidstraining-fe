import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../../../services/dataService.service';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-edit-delete-actions',
  templateUrl: './edit-delete-actions.component.html',
  styles: [
  ]
})
export class EditDeleteActionsComponent implements OnInit {

  @Input() value;
  @Input() rowData: any;

  @Input() navegateURL: string;
  @Input() param: string;
  @Input() authCodeEdit: string;
  @Input() authCodeDelete: string;
  renderValue;
  iconClass: string = "";
  parameter: string = "";
  @Input() apiCall: string;
  isOpenDrawer: boolean = false;

  @Output() save: EventEmitter<any> = new EventEmitter();
  @Output() openPopup: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, public service: DataService, public translate: TranslateService) {

  }

  ngOnInit(): void {
    this.parameter = this.rowData[this.param];
  }

  onClickEdit(value) {
    if (this.isOpenDrawer == true)
      this.openPopup.emit(this.rowData.id);
    else
      this.router.navigate(['./' + this.navegateURL + this.parameter])
  }

  onClickDelete(value) {
    this.deleteRows();
  }

  deleteRows = () => {
    Swal.fire({
      title: this.translate.instant('deleteConfirm'),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: this.translate.instant('yes'),
      cancelButtonText: this.translate.instant('no')
    }).then((result: any) => {
      if (result.value) {
        Swal.fire(
          this.translate.instant('remove'),
          this.translate.instant('removeDone'),
          'success'
        )
        this.confirmDelete();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          this.translate.instant('cancel'),
          this.translate.instant('cancelRemove'),
          'error'
        )
      }
    })
  }

  confirmDelete = () => {
    let ids = [];
    ids.push(this.rowData.id)
    this.service.post(this.apiCall, ids).subscribe(
      result => {
        this.save.emit(this.rowData.id);
      }
    )
  }
}
