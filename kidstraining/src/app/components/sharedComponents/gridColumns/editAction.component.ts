import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ViewCell } from 'ng2-smart-table';
import { DataService } from '../../../services/dataService.service';

@Component({
  selector: 'app-editAction',
  templateUrl: './editAction.component.html',
  styles: [
  ]
})
export class EditActionComponent implements ViewCell, OnInit {

  @Input() value;
  @Input() rowData: any;

  @Input() navegateURL: string;
  @Input() icon: string;
  @Input() param: string;
  @Input() authCode: string;
  renderValue;
  iconClass: string = "";
  parameter: string = "";
  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, public service: DataService) {

  }


  ngOnInit() {
    this.renderValue = this.value;
    this.iconClass = this.icon;
    this.parameter = this.rowData[this.param];
    console.log(this.renderValue)
  }

  onClick(value) {

    if (this.authCode == '0') {
     // this.service.checkPermissionsIsAdmin() ? null : this.router.navigate(['./PageNotAuthorized']);
      //if (this.service.checkPermissionsIsAdmin()) {
        this.router.navigate(['./' + this.navegateURL + this.parameter])
      //}
    }

    else {
      //if (this.service.checkAuths(this.authCode)) {
        this.router.navigate(['./' + this.navegateURL + this.parameter])
      //}
    }

  }

}
