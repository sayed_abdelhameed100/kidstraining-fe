import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DataService } from '../../../services/dataService.service';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @Output() getLayout: EventEmitter<string> = new EventEmitter();
  layout: string;
  lang;
  // userName = '';
  constructor(
    private router: Router,
    public translate: TranslateService,
    public service: DataService,
    private dataService: DataService
  ) {
    this.lang = localStorage.getItem('lang');
    // this.userName = this.dataService.userName;
  }

  ngOnInit(): void {
    // this.userName = localStorage.getItem('userName') || '';
  }

  getLang(lang) {
    this.layout = lang.value;
    this.getLayout.emit(this.layout);
  }

  get getUserName() {
    return localStorage.getItem('userName');
  }

  switchLang(lang) {
    console.log(lang);
    localStorage.removeItem('lang');
    this.translate.use(lang).toPromise();
    localStorage.setItem('lang', lang);
    if (lang == 'ar') {
      document.getElementsByTagName('html')[0].setAttribute('dir', 'rtl');
      document.getElementsByTagName('html')[0].setAttribute('lang', 'ar');
      document.getElementsByTagName('html')[0].classList.add('ar');
      document.getElementsByTagName('html')[0].classList.remove('en');
    } else {
      // document.getElementsByTagName('html')[0].removeAttribute('dir');
      document.getElementsByTagName('html')[0].setAttribute('dir', 'ltr');
      document.getElementsByTagName('html')[0].setAttribute('lang', 'en');
      document.getElementsByTagName('html')[0].classList.add('en');
      document.getElementsByTagName('html')[0].classList.remove('ar');
    }
    window.location.reload();
  }
}
