import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home-slider',
  templateUrl: './home-slider.component.html',
  styleUrls: ['./home-slider.component.scss']
})
export class HomeSliderComponent implements OnInit {
  dataslider = [];
 
  constructor() { 

    this.dataslider = [
      { id: 1, img:  "../../../../assets/img/carousel-1.jpg" },
      { id: 2, img:  "../../../../assets/img/carousel-2.jpg" },
    ]
  }

  ngOnInit(): void {
  }

}
