import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { DataService } from 'src/app/services/dataService.service';
import { Location } from '@angular/common';
import { ToastrService } from 'ngx-toastr';
import { ReTree } from 'ngx-device-detector';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
  user: any;
  userName: string = '';
  password: string = '';
  constructor(
    public service: DataService,
    private router: Router,
    private location: Location,
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {}

  login = () => {
    let obj: any = { userName: this.userName, password: this.password };
    if (obj.userName != '' && obj.password != '') {
      this.service.post('Login', obj).subscribe((result) => {
           console.log(result)
        let data = result as any;
        if (result == 1) {
          this.toastr.error('Login Failed', 'Login');
          return;
        } else if (data.msg == 'otp') {
          localStorage.setItem('userId', data?.user?.id);
          localStorage.setItem('phone', data?.user?.phone);
          this.toastr.error('Otp', 'Login');
          return;
          //open otp Page
        } else if (data.msg == 'notactive') {
          this.toastr.error(
            'this account is block please call support to active account',
            'Login'
          );
          return;
        } else {
          //if (data.isVer)
          if (this.service.checkPlanExpairDate(data.currentUserPlan.endDate)) {
            this.toastr.error('انتهاء اشتراك من فضلك جدد اشتراك');
            localStorage.setItem('userId', data.id);
            localStorage.setItem('phone', data.phone);
            return;
            //open renew popup to renew plane
          } else {
            localStorage.setItem('userId', data.id);
            localStorage.setItem('email', data.email);
            localStorage.setItem('token', data.token);
            localStorage.setItem('userName', data.userName);
            localStorage.setItem('lastName', data.confirmMsg);
            localStorage.setItem('phone', data.phone);
            // this.service.userName = data.userName;
            // this.service.userId = data.id;
            // if (this.service.isSignIn) {
            this.service.userName = data.userName;
            this.router.navigate(['./myProfile']);
          }
          // }
          // else {
          //   this.location.back();
          //   this.service.isSignIn = false;
          // }
        }
      });
    }
  };
}
