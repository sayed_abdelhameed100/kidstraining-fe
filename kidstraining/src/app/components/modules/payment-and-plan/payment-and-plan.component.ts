import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import Swal from 'sweetalert2';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-payment-and-plan',
  templateUrl: './payment-and-plan.component.html',
  styleUrls: ['./payment-and-plan.component.scss'],
})
export class PaymentAndPlanComponent implements OnInit {
  userPlans: any = [];
  currentPlan: any = {};
  modalRef?: BsModalRef;
  plans: any = [];
  plansOption: any = [];
  selectedPlan;
  startDatePlan: Date = new Date();

  renewOptions: string[] = ['Economy plan', 'Basic Plan', 'Golden Plan'];
  shownPlans: any[] = [];
  selectedDate: string;
  constructor(
    public service: DataService,
    private router: Router,
    private toastr: ToastrService,
    public translate: TranslateService,
    private modalService: BsModalService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    // this.getUserInfo();
    this.getAllPlans();
    //console.log(this.service.checkPlanExpairDate());
  }

  getUserInfo = () => {
    this.service.get('GetUserInfo').subscribe((result) => {
      let data = result as any;
      this.userPlans = data.userPlan.filter(x => x.isCurrentPlan == false);
      this.currentPlan = data.currentUserPlan;
    });
  }

  getAllPlans = () => {
    this.service.get('GetAllPlans').subscribe((result) => {
      let data = result as any;
      this.plans = data;
      data.map(s => {
        let obj: any = {};
        obj.value = s.id;
        obj.label = s.nameAr;
        this.plansOption.push(obj);
      });
    });
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  renew() {
    // this.shownPlans.push({
    //   planName: plan.value,
    //   startDate: this.changeDateFormate(new Date(duration.value)),
    //   endDate: this.getNextYearFromStartDate(duration.value),
    // });
    if (this.selectedPlan != null && this.startDatePlan != null) {
      let obj: any = {};
      obj.planId = this.selectedPlan;
      obj.StartDate = this.startDatePlan;
      this.service.post("AddUserPlan", obj).subscribe(
        result => {
          this.toastr.success("saveDone");
          this.modalRef.hide();
        });
    }
    else {
      this.service.vaildationBox();
      //this.toastr.error("inputAllFalids");
    }
  }

  // getNextYearFromStartDate(date: string) {
  //   let currentYear = new Date(date);
  //   return this.changeDateFormate(
  //     new Date(currentYear.setFullYear(currentYear.getFullYear() + 1))
  //   );
  // }

  // changeDateFormate(date: Date): any {
  //   return this.datePipe.transform(date);
  // }
}
