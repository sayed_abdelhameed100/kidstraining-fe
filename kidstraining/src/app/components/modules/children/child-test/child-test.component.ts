import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-child-test',
  templateUrl: './child-test.component.html',
  styleUrls: ['./child-test.component.scss'],
})
export class ChildTestComponent implements OnInit {
  testIndex: number = 0;
  tests: any[] = [];
  shownResult: any = [];
  constructor() {}

  ngOnInit(): void {
    this.fillTestsArray();
    this.shownResult = this.tests[this.testIndex];
  }

  fillTestsArray() {
    this.tests = [
      {
        videoSrc:
          '../../../../../assets/videos/Wegz - ElBakht - ويجز - البخت (Audio) prod. Rahal.mkv',
        description: `Lorem ipsum dolor sit amet consectetur, adipisicing elit. Suscipit praesentium at qui laborum possimus amet quo
          repellat repudiandae cum optio reiciendis, nobis maiores quis facilis quibusdam, accusantium voluptas inventore
          cupiditate. Quos molestiae ad, laudantium error voluptatum sint reiciendis maxime sunt a possimus quam ut quasi
          mollitia, nesciunt quas rem asperiores.`,
        result: '1',
      },
      {
        videoSrc:
          '../../../../../assets/videos/Ahmed Mekky , Aghla Men Al Yaqout -   أحمد مكى , أغلى من الياقوت , حصريا.mp4',
        description: `Lorem ipsum dolor, sit amet consectetur adipisicing elit. Omnis provident voluptatum molestiae officia quos
        inventore labore delectus placeat rerum iusto.`,
        result: '2',
      },
      {
        videoSrc: `../../../../../assets/videos/Wegz - Kan Nefsy - ويجز - كان نفسي prod. DJ Totti.mp4`,
        description: `    Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae facilis corrupti at quasi, corporis excepturi officia quas. Nostrum necessitatibus cum exercitationem corporis aliquam, ut labore molestiae asperiores numquam nihil odio.`,
        result: '3',
      },
    ];
  }
  changeChild(testIndexChange: number) {
    this.testIndex += testIndexChange;
    if (this.testIndex < 0) {
      this.testIndex = 0;
      return;
    }
    if (this.testIndex > this.tests.length - 1) {
      this.testIndex = this.tests.length - 1;
      return;
    }
    this.shownResult = this.tests[this.testIndex];
    console.log(this.shownResult);
  }
}
