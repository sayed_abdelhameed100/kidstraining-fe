import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import { ValidationService } from 'src/app/services/validation.service';
import Stepper from 'bs-stepper';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-add-edit-children',
  templateUrl: './add-edit-children.component.html',
  styleUrls: ['./add-edit-children.component.scss'],
})
export class AddEditChildrenComponent implements OnInit {
  private stepper: Stepper;
  submitted = false;
  editMode = false;
  formGroup = new FormGroup({});
  id: any = 0;
  sexData: any = [];
  keywordsLengthData: any = [];
  languageData: any = [];
  dayDiff;
  monthDiff;
  yearDiff;
  topExams: any = [];
  downExams: any = [];
  currentVideo: any;
  currentExam: any = [];
  currentExamClauses: any = [];
  wrongquestions = 0;
  baseUrl = environment.apiUrl + "/Images/";
  shownResult: any = [];
  examDegree: any = 1;
  totalexamDegree: any = 0;
  constructor(
    private formBuilder: FormBuilder,
    private activatedRoute: ActivatedRoute,
    public translate: TranslateService,
    private service: DataService,
    private router: Router,

  ) {
    if (this.activatedRoute.snapshot.paramMap.get('id') !== null) {
      if (this.activatedRoute.snapshot.paramMap.get('id') != '0') {
        this.editMode = true;
        this.id = this.activatedRoute.snapshot.paramMap.get('id');
      }
    }



    this.getAllSexData();
    this.getAllKeywordsLengthData();
    // this.getAllLanguageData();
    this.languageData = [
      {
        value: 'ar',
        label: this.translate.currentLang == 'ar' ? 'العربية' : 'Arabic',
      },
      {
        value: 'en',
        label: this.translate.currentLang == 'ar' ? 'الانجليزية' : 'English',
      },
    ];






    this.service.get("SelectChildPerformanceCeilingExamsByAge?ExamAge=37").subscribe(res => {
      console.log(res)
      this.topExams = res;
      this.currentExam = res[0];
      this.currentExamClauses = this.currentExam["clauses"][0];
      this.currentVideo = this.currentExamClauses.question[0];

    })

  }




  next() {
    this.stepper.next();
  }

  ngOnInit(): void {
    this.creationForm();
    if (this.editMode) this.getById();
    this.stepper = new Stepper(document.querySelector('#stepper1'), {
      linear: false,
      animation: true
    })
  }

  getAllSexData = () => {
    this.service
      .get('GetAllDefaultDataForDropDown?TypeId=1')
      .subscribe((result) => {
        this.sexData = result;
      });
  };

  getAllKeywordsLengthData = () => {
    this.service
      .get('GetAllDefaultDataForDropDown?TypeId=2')
      .subscribe((result) => {
        this.keywordsLengthData = result;
      });
  };

  // getAllLanguageData = () => {
  //   this.service.get("GetAllDefaultDataForDropDown?TypeId=3").subscribe(
  //     result => {
  //       this.languageData = result;
  //     }
  //   )
  // }

  creationForm = () => {
    this.formGroup = this.formBuilder.group({
      name: [null, [Validators.required]],
      childLang: [null, [Validators.required]],
      sex: [null, [Validators.required]],
      //childLang: [this.translate.currentLang],
      //sex: [null],
      birthdate: [null, [Validators.required]],
      diagnosis: [null],
      ageInMonth: [null],
      keyWordLengthId: [null],
      id: [this.editMode ? this.activatedRoute.snapshot.paramMap.get('id') : 0],
    });
  };

  get f() {
    return this.formGroup.controls;
  }

  getById = () => {
    this.service.get('GetChildrenById?id=' + this.id).subscribe((result) => {
      let data = result as any;
      this.formGroup.patchValue({
        name: data.name,
        childLang: data.childLang,
        sex: data.sex,
        birthdate: new Date(data.birthdate),
        diagnosis: data.diagnosis,
        ageInMonth: data.ageInMonth,
        keyWordLengthId: data.keyWordLengthId,
        id: data.id,
      });
    });
  };

  onBirthDateChange = (value) => {
    if (value) {
      var oldDate = value;
      this.dateDiff(oldDate, new Date());
    }
  };


  dateDiff(startingDate, endingDate) {
    var startDate = new Date(new Date(startingDate).toISOString().substr(0, 10));
    if (!endingDate) {
      endingDate = new Date().toISOString().substr(0, 10); // need date in YYYY-MM-DD format
    }
    var endDate = new Date(endingDate);
    if (startDate > endDate) {
      var swap = startDate;
      startDate = endDate;
      endDate = swap;
    }
    var startYear = startDate.getFullYear();
    var february = (startYear % 4 === 0 && startYear % 100 !== 0) || startYear % 400 === 0 ? 29 : 28;
    var daysInMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

    var yearDiff = endDate.getFullYear() - startYear;
    var monthDiff = endDate.getMonth() - startDate.getMonth();
    if (monthDiff < 0) {
      yearDiff--;
      monthDiff += 12;
    }
    var dayDiff = endDate.getDate() - startDate.getDate();
    if (dayDiff < 0) {
      if (monthDiff > 0) {
        monthDiff--;
      } else {
        yearDiff--;
        monthDiff = 11;
      }
      dayDiff += daysInMonth[startDate.getMonth()];
    }
    console.log(dayDiff, monthDiff, yearDiff)
    this.yearDiff = yearDiff;
    this.monthDiff = monthDiff;
    this.dayDiff = dayDiff;
    this.f.ageInMonth.setValue(monthDiff + (yearDiff * 12));
  }

  submit = () => {
    this.submitted = true;
    if (this.formGroup.invalid) {
      this.service.vaildationBox();
      return;
    }

    let urlApi = this.editMode ? 'EditChildren' : 'AddChildren';
    this.service.post(urlApi, this.formGroup.value).subscribe((result) => {
      this.service.confirmBox();
      let data = result as any;
      this.id = data?.id;
      //this.router.navigate(['./myProfile']);
      if (!this.editMode) {
        this.next();

        this.service.get("SelectChildPerformanceCeilingExamsByAge?Age=" + data.ageInMonth).subscribe(res => {
          console.log(res)
          this.topExams = res;
          this.currentExam = res[0];
          this.currentExamClauses = this.currentExam["clauses"][0];
          this.currentVideo = this.currentExamClauses.question[0];
        })

      }
    });
  };


  nextExam = () => {
    console.log(this.examDegree)
    this.totalexamDegree = this.totalexamDegree + this.examDegree;
    this.saveExamDegree();

    let currentExamIndex = this.topExams.findIndex(s => s.id == this.currentExam.id);

    let currentExamClausesIndex = this.currentExam.clauses.findIndex(s => s.id == this.currentExamClauses.id);

    let currentVideoIndex = this.currentExamClauses.question.findIndex(s => s.id == this.currentVideo.id);

    if (currentVideoIndex + 1 == this.currentExamClauses.question.length) {
      if (currentExamClausesIndex + 1 == this.currentExam.clauses.length) {

        let accaptDegree = (this.currentExam.degree / 3) * 2
        if (this.totalexamDegree <= accaptDegree) {
          this.wrongquestions = this.wrongquestions + 1;
          this.totalexamDegree = 0;
          if (this.wrongquestions == 3) {
            alert("تريد اخد سقف ")
          }
        }
        this.currentExam = this.topExams[currentExamIndex + 1]
        currentExamClausesIndex = -1;

      }
      this.currentExamClauses = this.currentExam.clauses[currentExamClausesIndex + 1];
      this.currentVideo = this.currentExamClauses.question[0];
    }
    else {
      this.currentVideo = this.currentExamClauses.question[currentVideoIndex + 1]
    }
  }

  saveExamDegree = () => {
    this.service.post("",).subscribe(res => {

    })
  }

}
