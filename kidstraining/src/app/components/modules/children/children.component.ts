import { DatePipe } from '@angular/common';
import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import Swal from 'sweetalert2';
import { DataService } from '../../../services/dataService.service';
import { EditDeleteActionsComponent } from '../../sharedComponents/gridColumns/edit-delete-actions/edit-delete-actions.component';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.scss'],
})
export class ChildrenComponent implements OnInit {
  tableData: any = [];
  filterData: any = [];
  searchText: string = '';
  selectedRows: any[] = [];
  settings: any = this.service.loadTableSettings();
 
  constructor(
    public service: DataService,
    private router: Router,
    public translate: TranslateService,
    private datePipe:DatePipe
  ) {
    this.service.get('GetAllChildrens').subscribe((result) => {
      let data = result as any;
      this.tableData = data;
      this.onSearch();
      this.settings = this.service.loadTableSettings(this.setTableColumns());
    });
  }

  setTableColumns = () => {
    let columns = {
      name: {
        title: this.translate.instant('Child Name'),
        type: 'text',
      },

      birthdate: {
        title: this.translate.instant('birthdate'),
        valuePrepareFunction: (birthdate) => {
          var raw = new Date(birthdate);
          var formatted = this.datePipe.transform(raw, 'yyyy-MM-dd');
          return formatted;
        },
        type: 'text',
        width: '120px'
      },
      ageInMonth: {
        title: this.translate.instant('ageInMonth'),
        type: 'text',
      },
      sex: {
        title: this.translate.instant('gender'),
        type: 'text',
      },

      actions: {
        title: this.translate.instant('actions'),
        type: 'custom',
        editable: true,
        filter: false,
        width: '30px',
        renderComponent: EditDeleteActionsComponent,
        valuePrepareFunction: (cell, row) => row,
        onComponentInitFunction: (instance) => {
          instance.param = 'id';
          instance.navegateURL = 'AddEditChild/';
          instance.authCodeEdit = '11';
          instance.authCodeDelete = '12';
          instance.apiCall = 'DeleteChildrenById';
          instance.save.subscribe((row) => {
            this.updateTable(row);
          });
        },
      },
    };
    return columns;
  };

  onSearch(query: any = '') {
    this.filterData = this.service.onSearch(query, this.tableData);
  }

  updateTable = (id) => {
    this.tableData = this.tableData.filter((s) => s.id != id);
    this.filterData = this.filterData.filter((s) => s.id != id);
  };

  ngOnInit(): void {
    // this.service.checkPlanExpairDate() ? null : this.settings.actions.custom = [];
  }

  deleteRows = () => {
    Swal.fire({
      title: this.translate.instant('deleteConfirm'),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: this.translate.instant('yes'),
      cancelButtonText: this.translate.instant('no'),
    }).then((result: any) => {
      if (result.value) {
        Swal.fire(
          this.translate.instant('remove'),
          this.translate.instant('removeDone'),
          'success'
        );
        this.confirmDelete();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire(
          this.translate.instant('cancel'),
          this.translate.instant('cancelRemove'),
          'error'
        );
      }
    });
  };

  confirmDelete = () => {
    let ids = this.selectedRows.map((s) => {
      return s.id;
    });
    console.log(ids);
    this.service.post('DeleteChildrensByIds', ids).subscribe((result) => {
      this.tableData = this.tableData.filter(
        (s: any) => !this.selectedRows.includes(s)
      );
      this.filterData = this.filterData.filter(
        (s: any) => !this.selectedRows.includes(s)
      );
      this.selectedRows = [];
    });
  };

  onUserRowSelect(event: any) {
    this.selectedRows = event.selected;
  }
}
