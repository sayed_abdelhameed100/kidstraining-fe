import { Component, Input, OnInit, TemplateRef } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import Swal from 'sweetalert2';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { DatePipe } from '@angular/common';
@Component({
  selector: 'app-renew-plan-popup',
  templateUrl: './renew-plan-popup.component.html',
  styleUrls: ['./renew-plan-popup.component.scss']
})
export class RenewPlanPopupComponent implements OnInit {

  userPlans: any = [];
  currentPlan: any = {};
  modalRef?: BsModalRef;
  plans: any = [];
  plansOption: any = [];
  selectedPlan;
  startDatePlan: Date = new Date();

  shownPlans: any[] = [];
  selectedDate: string;
  constructor(
    public service: DataService,
    private router: Router,
    private toastr: ToastrService,
    public translate: TranslateService,
    private modalService: BsModalService,
    private datePipe: DatePipe
  ) { }

  ngOnInit(): void {
    this.getAllPlans();
  }



  getAllPlans = () => {
    this.service.get('GetAllPlans').subscribe((result) => {
      let data = result as any;
      this.plans = data;
      data.map(s => {
        let obj: any = {};
        obj.value = s.id;
        obj.label = s.nameAr;
        this.plansOption.push(obj);
      });
    });
  }


  renew() {
    if (this.selectedPlan != null && this.startDatePlan != null) {
      let obj: any = {};
      obj.planId = this.selectedPlan;
      obj.StartDate = this.startDatePlan;
      this.service.post("AddUserPlan", obj).subscribe(
        result => {
          this.toastr.success("saveDone");
          this.router.navigate(['./login']);
        });
    }
    else {
      this.service.vaildationBox();
      //this.toastr.error("inputAllFalids");
    }
  }

}

