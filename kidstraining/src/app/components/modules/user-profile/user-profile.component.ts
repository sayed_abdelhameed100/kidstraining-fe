import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import Swal from 'sweetalert2';
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  currentTab = 1;
  constructor(public service: DataService, private router: Router, private toastr: ToastrService, public translate: TranslateService) { }

  ngOnInit(): void {
  }

  logOut = () => {
    //need to confirm message
    localStorage.clear();
    this.service.userName = "";
    this.router.navigate(['./home']);
  }

  confirmlogOut = () => {
    Swal.fire({
      title: this.translate.instant('confirmLogOut'),
      text: '',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: this.translate.instant('yes'),
      cancelButtonText: this.translate.instant('no')
    }).then((result: any) => {
      if (result.value) {

        this.logOut();
      } else if (result.dismiss === Swal.DismissReason.cancel) {
      }
    })
  }

  changeTab = (tabNum) => {
    this.service.currentTab = tabNum;
  }
}
