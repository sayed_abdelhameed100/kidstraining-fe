import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import { ValidationService } from 'src/app/services/validation.service';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss'],
})
export class EditProfileComponent implements OnInit {
  filesLogo: File[] = [];
  logoUrl: string = null;
  imageURLBaseLogo = '';
  userInfo: any = {};
  userForm: FormGroup = new FormGroup({});
  constructor(
    private router: Router,
    private toastr: ToastrService,
    private service: DataService,
    public translate: TranslateService
  ) {}

  ngOnInit(): void {
    // this.getUserInfo();
  }

  getUserInfo = () => {
    this.service.get('GetUserInfo').subscribe((result) => {
      console.log('result', result);
      this.userInfo = result;
    });
  };

  onSelectLogo(event) {
    console.log('event', event);
    this.filesLogo.push(...event.addedFiles);
    this.userInfo.logoUrl = this.filesLogo[0].name;
    console.log('this.filesLogo', this.filesLogo[0].name);
    // this.service
    //   .uploadFile('UploadUserLogo', this.filesLogo[0])
    //   .subscribe((result) => {
    //     console.log('res', result);
    //     this.logoUrl = environment.apiUrl + result;
    //     this.imageURLBaseLogo = result as string;
    //   });
  }

  onRemoveLogo(event) {
    console.log(event);
    this.filesLogo.splice(this.filesLogo.indexOf(event), 1);
  }

  saveUser = () => {
    this.service.post('EditUser', this.userInfo).subscribe((result) => {
      this.toastr.success('save done');
      //this.router.navigate(['./Login']);
    });
  };
}
