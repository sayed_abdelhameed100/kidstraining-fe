import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import { ValidationService } from 'src/app/services/validation.service';
import { SearchCountryField, CountryISO, PhoneNumberFormat } from 'ngx-intl-tel-input';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  signinForm: FormGroup = new FormGroup({});
  isSubmit = false;
  isOTPOpened: boolean = false;
  secondsPassed: number = undefined;
  display: any;
  timeOut: boolean = false;
  otp;
  userId;

  SearchCountryField = SearchCountryField;
  CountryISO = CountryISO;
  PhoneNumberFormat = PhoneNumberFormat;
  preferredCountries: CountryISO[] = [CountryISO.Egypt, CountryISO.Oman, CountryISO.SaudiArabia, CountryISO.Qatar, CountryISO.UnitedArabEmirates];


  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private service: DataService,
    public translate: TranslateService
  ) { }

  ngOnInit(): void {
    this.buildForm();
  }

  buildForm() {
    this.signinForm = this.formBuilder.group(
      {
        userName: ['', Validators.required],
        email: ['', [Validators.required, Validators.email]],
        phone: [undefined, [Validators.required]],
        password: ['', Validators.required],
        confirmPassword: ['', Validators.required],
        nameAR: [''],
        nameEn: [''],
        address: [''],
      },
      {
        validator: this.ConfirmedValidator('password', 'confirmPassword'),
      }
    );
  }

  ConfirmedValidator(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (
        matchingControl.errors &&
        !matchingControl.errors.confirmedValidator
      ) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
        console.log(matchingControl.errors);
      } else {
        matchingControl.setErrors(null);
      }
    };
  }

  get f() {
    return this.signinForm.controls;
  }

  checkUserNameIsExist = () => {
    this.service
      .get('CheckUser?UserName=' + this.f.userName.value.replace(/ /g, ''))
      .subscribe((result) => {
        if (result !== 0) {
          this.f.userName.setValue('');
          this.toastr.error(this.translate.instant('userNameAlreadyExist'));
        }
      });
  };

  checkEmailIsExist = () => {
    this.service
      .get('CheckUserEmail?Email=' + this.f.email.value.replace(/ /g, ''))
      .subscribe((result) => {
        if (result !== 0) {
          this.f.email.setValue('');
          this.toastr.error(this.translate.instant('emailAlreadyExist'));
        }
      });
  };

  checkPhoneIsExist = () => {
    if (!this.f.phone.errors) {
      console.log(this.f.phone.value)
      this.service.get('CheckUserNumber?Phone=' + this.f.phone.value.e164Number.substring(1)).subscribe((result) => {
        if (result !== 0) {
          this.f.phone.setValue('');
          this.toastr.error(this.translate.instant('phoneAlreadyExist'));
        }
      });
    }
  };

  onSubmit() {
    this.isSubmit = true;
    if (this.signinForm.valid) {
      let data = this.signinForm.value;
      data.phone = this.f.phone.value.e164Number
      this.service.post('AddUser', data).subscribe((result) => {
        let data = result as any;
        this.isOTPOpened = true;
        this.userId = data.id;
        this.timer(2);
        window.scrollTo(0, 0);
        // this.router.navigate(['./login']);
      });
    }
  }


  timer(minute) {
    // let minute = 1;
    let seconds: number = minute * 60;
    let textSec: any = "0";
    let statSec: number = 60;

    const prefix = minute < 10 ? "0" : "";

    const timer = setInterval(() => {
      seconds--;
      if (statSec != 0) statSec--;
      else statSec = 59;

      if (statSec < 10) {
        textSec = "0" + statSec;
      } else textSec = statSec;

      this.display = `${prefix}${Math.floor(seconds / 60)}:${textSec}`;

      if (seconds == 0) {
        console.log("finished");
        clearInterval(timer);
        this.timeOut = true;
      }
    }, 1000);
  }

  reSendOtp = () => {
    //this.timer(120);
    this.resendOtpCode();
  }


  checkOtpCode = () => {
    this.service.get("CheckOtp?UserId=" + this.userId + "&Otp=" + this.otp).subscribe(res => {
      let result = res as any;
      if (result == true) {
        this.router.navigate(['./login']);
      }
      else {
        this.toastr.error("otp code not correct", "Authentication")
        // alert(result.message)
      }
    });

  }


  resendOtpCode = () => {
    this.service.get("ReSendOtp?Phone=" + this.f.phone.value).subscribe(result => {
      //console.log("checkOtpCode", result)
      this.timeOut = false;

      this.timer(2);

    });

  }

  changePreferredCountries() {
    this.preferredCountries = [CountryISO.Egypt, CountryISO.Canada];
  }
}
