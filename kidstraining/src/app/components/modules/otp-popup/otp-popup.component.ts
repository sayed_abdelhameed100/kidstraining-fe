import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { NavigationEnd, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { ToastrService } from 'ngx-toastr';
import { DataService } from 'src/app/services/dataService.service';
import { ValidationService } from 'src/app/services/validation.service';
@Component({
  selector: 'app-otp-popup',
  templateUrl: './otp-popup.component.html',
  styleUrls: ['./otp-popup.component.scss']
})
export class OtpPopupComponent implements OnInit {
  isOTPOpened: boolean = false;
  secondsPassed: number = undefined;
  display: any;
  timeOut: boolean = false;
  otp;
  userId;
  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private service: DataService,
    public translate: TranslateService
  ) {
    this.timer(2);
  }

  ngOnInit(): void {
  }

  timer(minute) {
    // let minute = 1;
    let seconds: number = minute * 60;
    let textSec: any = "0";
    let statSec: number = 60;

    const prefix = minute < 10 ? "0" : "";

    const timer = setInterval(() => {
      seconds--;
      if (statSec != 0) statSec--;
      else statSec = 59;

      if (statSec < 10) {
        textSec = "0" + statSec;
      } else textSec = statSec;

      this.display = `${prefix}${Math.floor(seconds / 60)}:${textSec}`;

      if (seconds == 0) {
        console.log("finished");
        clearInterval(timer);
        this.timeOut = true;
      }
    }, 1000);
  }

  reSendOtp = () => {
    //this.timer(120);
    this.resendOtpCode();
  }


  checkOtpCode = () => {
    this.service.get("CheckOtp?UserId=" + localStorage.getItem("userId") + "&Otp=" + this.otp).subscribe(res => {
      let result = res as any;
      if (result == true) {
        this.router.navigate(['./login']);
      }
      else {
        this.toastr.error("otp code not correct", "Authentication")
        // alert(result.message)
      }
    });

  }


  resendOtpCode = () => {
    this.service.get("ReSendOtp?Phone=" + localStorage.getItem("phone")).subscribe(result => {
      //console.log("checkOtpCode", result)
      this.timeOut = false;

      this.timer(2);

    });

  }

}
