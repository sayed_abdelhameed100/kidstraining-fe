import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'time24to12Pipe'
})
export class Time24to12PipePipe implements PipeTransform {
  transform(time: any): any {

    var time24To12 = function (a: any) {
      return (new Date("1955-11-05T" + a)).toLocaleTimeString('en-US', {
      
        hour: "numeric",
        minute: "numeric",
        hour12: true
      });
    };

    return time24To12(time);
  }

}
