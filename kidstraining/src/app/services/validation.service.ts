import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  numericOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode == 49 ||
      charCode == 50 || charCode == 51 || charCode == 52 ||
      charCode == 53 || charCode == 54 || charCode == 55 ||
      charCode == 56 || charCode == 57 || charCode == 48) {
      return true;
    } else {
      return false;
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    else return true;
  }
}
