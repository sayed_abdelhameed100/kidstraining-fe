import { Injectable, Optional } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import Swal from 'sweetalert2';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { DeviceDetectorService } from 'ngx-device-detector';
import jwt_decode from 'jwt-decode';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  userName = '';
  currentTab = 1;
  constructor(
    private http: HttpClient,
    private translate: TranslateService,
    private router: Router,
    private datePipe: DatePipe
  ) {
    this.userName = localStorage.getItem('userName') || '';
  }

  get = (url: string) => {
    return this.http.get(environment.apiUrl + 'api/' + url);
  };

  post = (url: string, params: any = null) => {
    return this.http.post(environment.apiUrl + 'api/' + url, params);
  };

  uploadFile = (url: string, files) => {
    let fileToUpload = <File>files;
    // let fileToUpload = <File>files;
    const formData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    return this.http.post(environment.apiUrl + 'api/' + url, formData);
  };

  vaildationBox() {
    Swal.fire({
      title: this.translate.instant('error'),
      text: this.translate.instant('requiredInputAll'),
      icon: 'error',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('ok'),
    }).then((result: any) => {
      if (result.value) {
      }
    });
  }

  confirmBox() {
    Swal.fire({
      title: this.translate.instant('save'),
      text: this.translate.instant('saveDone'),
      icon: 'success',
      showCancelButton: false,
      confirmButtonText: this.translate.instant('ok'),
    }).then((result: any) => {
      if (result.value) {
      }
    });
  }

  loadTableSettings = (colums: any = [], selectMode = true) => {
    var settings = {};
    if (selectMode) {
      settings = {
        selectMode: 'multi',
        hideSubHeader: true,
        actions: {
          add: false,
          edit: false,
          delete: false,
        },
        columns: colums,

        attr: {
          class:
            'table data-thumb-view dataTable no-footer dt-checkboxes-select form-check-input',
        },
      };
    } else {
      settings = {
        hideSubHeader: true,
        actions: {
          add: false,
          edit: false,
          delete: false,
        },
        columns: colums,

        attr: {
          class:
            'table data-thumb-view dataTable no-footer dt-checkboxes-select form-check-input',
        },
      };
    }
    return settings;
  };

  onSearch(query: string = '', tableData) {
    if (query == '') {
      return tableData;
    } else {
      return tableData.filter((item) => {
        return Object.keys(item).some((key) => {
          return String(item[key]).toLowerCase().includes(query.toLowerCase());
        });
      });
    }
  }

  getUserIfo = () => {
    var token = localStorage.getItem('token');
    try {
      var decoded = jwt_decode(token);
      return decoded['userId'];
    } catch (error) {}
  };

  checkPlanExpairDate = (date) => {
    //var token = localStorage.getItem("token");

    //var decoded = jwt_decode(token);

    let datenow = this.changeDateFormate(new Date());
    //console.log(datenow, decoded['expairDate'])
    if (date < datenow) return true;
    else return false;
  };
  changeDateFormate(date: Date): any {
    return this.datePipe.transform(date, 'yyyy-dd-MM');
  }
}
