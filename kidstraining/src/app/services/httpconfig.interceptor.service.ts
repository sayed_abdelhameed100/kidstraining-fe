import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';

import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { throwError, Observable, BehaviorSubject, of, finalize } from "rxjs";
import { catchError, filter, take, switchMap } from "rxjs/operators";
@Injectable({
    providedIn: 'root'
})
export class Httpconfig implements HttpInterceptor {

    constructor(private router: Router,
        private spinner: NgxSpinnerService,
    ) {

    }




    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        let words = req.url.split('/');
        console.log("reqsss", words)
        let request: any;

        request = req.clone({
            setHeaders: {
                'Authorization': `Bearer ${localStorage.getItem('token') || ''}`,
                //'Content-Type': 'application/json;',
                'Lang': `${localStorage.getItem('lang') || ''}`,
            },
            //withCredentials: true
        });

        this.spinner.show();
        return next.handle(request).pipe(

            catchError((err, caught: Observable<HttpEvent<any>>) => {

                if (err instanceof HttpErrorResponse && err.status == 401) {
                    localStorage.removeItem("id");
                    localStorage.removeItem("UserName");
                    localStorage.removeItem("token");
                    this.router.navigate(['./login'])
                    return of(err as any);
                }
                else if (err instanceof HttpErrorResponse && err.status == 500) {
                    this.spinner.hide();
                    // Swal.fire({
                    //   position: 'center',
                    //   icon: 'error',
                    //   title: 'Some Error Happened',
                    //   showConfirmButton: false,
                    //   timer: 500
                    // })

                    return of(err as any);
                }
                else if (err instanceof HttpErrorResponse && err.status == 404) {
                    this.spinner.hide();
                }

                else {
                    this.spinner.hide();
                    // Swal.fire({
                    //   position: 'center',
                    //   icon: 'error',
                    //   title: 'Some Error Happened',
                    //   showConfirmButton: false,
                    //   timer: 500
                    // })
                }

                throw err;
            }),
            finalize(() => {
                this.spinner.hide();
            })


        );
    }
}

