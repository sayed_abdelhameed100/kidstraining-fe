using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;

namespace kidsTraining_DataService.Repository
{
    public class ChildrenRepository : GenericRepository<Child>, IChildrenRepository
    {

        private IMapper _mapper;
        private readonly IHttpContextAccessor _accessor;
        private readonly string Lang = "ar";
        private readonly int? UserId = null;
        public ChildrenRepository(kidsTrainingDBContext c, IMapper m, IHttpContextAccessor accessor) : base(c)
        {
            _accessor = accessor;
            Lang = _accessor.HttpContext.Request.Headers["Lang"];
            _mapper = m;
        }
        public DTOChildren AddItem(DTOChildren Item)
        {
            var sss = _accessor.HttpContext.User;
            int userId = Convert.ToInt32(_accessor.HttpContext.User.FindFirst(c => c.Type == "userId").Value);
            var result = _mapper.Map<Child>(Item);
            result.UserId = userId;
            Add(result);
            Save();
            Item.Id = result.Id;
            var examId = Context.Exams.Where(x => x.ExamAge == (Item.AgeInMonth-12) && x.ParenttId == null).OrderBy(x => x.SortNumber).FirstOrDefault();
            Context.ChildTestPerformances.Add(new ChildTestPerformance() { ChildId = Item.Id, IsCurrentTest = true, ExamStartId = examId.Id, });
            Save();
            return Item;

        }
        public DTOChildren EditItem(DTOChildren Item)
        {
            int userId = Convert.ToInt32(_accessor.HttpContext.User.FindFirst(c => c.Type == "userId").Value);
            var result = _mapper.Map<Child>(Item);
            result.UserId = userId;
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOChildren> Items)
        {
            foreach (DTOChildren Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOChildren Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOChildren> SelectAll()
        {

            int userId = Convert.ToInt32(_accessor.HttpContext.User.FindFirst(c => c.Type == "userId").Value);
            var list = new List<DTOChildren>();
            list = FindBy(x=>x.UserId== userId).Select(q => new DTOChildren
            {
                Id = q.Id,
                Name = q.Name,
                ChildLang = q.ChildLang,
                Sex = q.Sex,
                Diagnosis = q.Diagnosis,
                Birthdate = q.Birthdate,
                AgeInMonth = q.AgeInMonth,
                UserId=q.UserId,
                KeyWordLengthId=q.KeyWordLengthId

            }).ToList();
            return list;
        }
        public DTOChildren SelectById(int id)
        {
            var list = new DTOChildren();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOChildren
                    {
                        Id = q.Id,
                        Name = q.Name,
                        ChildLang = q.ChildLang,
                        Sex = q.Sex,
                        Diagnosis = q.Diagnosis,
                        Birthdate = q.Birthdate,
                        AgeInMonth = q.AgeInMonth,
                        UserId = q.UserId,
                        KeyWordLengthId = q.KeyWordLengthId
                    }).FirstOrDefault();
            return list;
        }

    }
}

