using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;


namespace kidsTraining_DataService.Repository
{
    public class ExamRepository : GenericRepository<Exam>, IExamRepository
    {

        private IMapper _mapper;
        public ExamRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOExam AddItem(DTOExam Item)
        {
            var result = _mapper.Map<Exam>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOExam EditItem(DTOExam Item)
        {
            var result = _mapper.Map<Exam>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOExam> Items)
        {
            foreach (DTOExam Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOExam Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOExam> SelectAll()
        {
            var list = new List<DTOExam>();
            list = GetAll().Select(q => new DTOExam
            {
                Id = q.Id,
                ExamCode = q.ExamCode,
                NameEn = q.NameEn,
                NameAr = q.NameAr,
                ExamAge = q.ExamAge,
                Degree = q.Degree,
                ParenttId = q.ParenttId,
                SortNumber = q.SortNumber,

            }).ToList();
            return list;
        }
        public DTOExam SelectById(int id)
        {
            var list = new DTOExam();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOExam
                    {
                        Id = q.Id,
                        ExamCode = q.ExamCode,
                        NameEn = q.NameEn,
                        NameAr = q.NameAr,
                        ExamAge = q.ExamAge,
                        Degree = q.Degree,
                        ParenttId = q.ParenttId,
                        SortNumber = q.SortNumber,

                    }).FirstOrDefault();
            return list;
        }

        public List<DTOExam> SelectExamsByAge(int? ExamAge)
        {
            int? currentExamAge = ExamAge - 12;
            var list = new List<DTOExam>();
            list = FindBy(x => x.ExamAge >= currentExamAge && x.ParenttId == null).Select(q => new DTOExam
            {
                Id = q.Id,
                ExamCode = q.ExamCode,
                NameEn = q.NameEn,
                NameAr = q.NameAr,
                ExamAge = q.ExamAge,
                Degree = q.Degree,
                ParenttId = q.ParenttId,
                SortNumber = q.SortNumber,


                Question = q.ExamQuestions.Where(x => x.ExamId == q.Id).Select(s => new DTOExamQuestion()
                {
                    Id = s.Id,
                    ExamQuestionCode = s.ExamQuestionCode,
                    NameAr = s.NameAr,
                    NameEn = s.NameEn,
                    SortNumber = s.SortNumber,
                    DescriptionAr = s.DescriptionAr,
                    DescriptionEn = s.DescriptionEn,
                    VideoUrl = s.VideoUrl
                }).OrderBy(x => x.SortNumber).ToList()
            }).OrderBy(x => x.SortNumber).ToList();

            foreach (var item in list)
            {
                item.Clauses = FindBy(x => x.ParenttId != null && x.ParenttId == item.Id).Select(q => new DTOExam
                {
                    Id = q.Id,
                    ExamCode = q.ExamCode,
                    NameEn = q.NameEn,
                    NameAr = q.NameAr,
                    ExamAge = q.ExamAge,
                    Degree = q.Degree,
                    ParenttId = q.ParenttId,
                    SortNumber = q.SortNumber,

                    Question = q.ExamQuestions.Where(x => x.ExamId == q.Id).Select(s => new DTOExamQuestion()
                    {
                        Id = s.Id,
                        ExamQuestionCode = s.ExamQuestionCode,
                        NameAr = s.NameAr,
                        NameEn = s.NameEn,
                        SortNumber = s.SortNumber,
                        DescriptionAr = s.DescriptionAr,
                        DescriptionEn = s.DescriptionEn,
                        VideoUrl = s.VideoUrl
                    }).OrderBy(x => x.SortNumber).ToList()
                }).OrderBy(x => x.SortNumber).ToList();


            }
            list.Select(c =>
            {
                c.Clauses = c.Question.Count > 0 ? new List<DTOExam>() { new DTOExam() {
                    SortNumber=c.SortNumber,
                    Degree=c.Degree,
                    ExamAge=c.ExamAge,
                    Clauses=c.Clauses,
                    ExamCode=c.ExamCode,
                    Id=c.Id,
                    Name=c.Name,
                    NameAr=c.NameAr,
                    NameEn=c.NameEn,
                    ParenttId=c.ParenttId,
                    Question=c.Question.ToList()} } : c.Clauses; return c;
            }).ToList();
            return list;
        }

        public int? FirstChildTest(int? ExamAge)
        {

            //if (ExamAge < 25)
            //{
            //    return FindBy(x => x.ParenttId == null).OrderBy(x => x.SortNumber).FirstOrDefault().ExamAge;
            //}
            //else if (ExamAge > 95)
            //{
            //    return FindBy(x => x.ParenttId == null).OrderByDescending(x => x.SortNumber).FirstOrDefault().ExamAge;
            //}
            //else
            //{
            //    var age = FindBy(x => x.ExamAge == ExamAge && x.ParenttId == null).OrderByDescending(x => x.SortNumber).FirstOrDefault();
            //    return age.ExamAge;

            //}

            switch (ExamAge)
            {
                case < 25:
                    return FindBy(x => x.ParenttId == null).OrderBy(x => x.SortNumber).FirstOrDefault()?.ExamAge;
                    break;

                case > 95:
                    return FindBy(x => x.ParenttId == null).OrderByDescending(x => x.SortNumber).FirstOrDefault()?.ExamAge;
                    break;

                default:

                    return FindBy(x => x.ExamAge == ExamAge && x.ParenttId == null).OrderByDescending(x => x.SortNumber).FirstOrDefault()?.ExamAge;
                    break;
            }


        }

        public List<DTOExam> SelectChildPerformanceCeilingExamsByAge(int? ExamAge)
        {
            int? currentExamAge = ExamAge - 12;
            var list = new List<DTOExam>();
            list = FindBy(x => x.ExamAge >= currentExamAge && x.ParenttId == null).Select(q => new DTOExam
            {
                Id = q.Id,
                ExamCode = q.ExamCode,
                NameEn = q.NameEn,
                NameAr = q.NameAr,
                ExamAge = q.ExamAge,
                Degree = q.Degree,
                ParenttId = q.ParenttId,
                SortNumber = q.SortNumber,


                Question = q.ExamQuestions.Where(x => x.ExamId == q.Id).Select(s => new DTOExamQuestion()
                {
                    Id = s.Id,
                    ExamQuestionCode = s.ExamQuestionCode,
                    NameAr = s.NameAr,
                    NameEn = s.NameEn,
                    SortNumber = s.SortNumber,
                    DescriptionAr = s.DescriptionAr,
                    DescriptionEn = s.DescriptionEn,
                    VideoUrl = s.VideoUrl
                }).OrderBy(x => x.SortNumber).ToList()
            }).OrderBy(x => x.SortNumber).ToList();

            foreach (var item in list)
            {
                item.Clauses = FindBy(x => x.ParenttId != null && x.ParenttId == item.Id).Select(q => new DTOExam
                {
                    Id = q.Id,
                    ExamCode = q.ExamCode,
                    NameEn = q.NameEn,
                    NameAr = q.NameAr,
                    ExamAge = q.ExamAge,
                    Degree = q.Degree,
                    ParenttId = q.ParenttId,
                    SortNumber = q.SortNumber,

                    Question = q.ExamQuestions.Where(x => x.ExamId == q.Id).Select(s => new DTOExamQuestion()
                    {
                        Id = s.Id,
                        ExamQuestionCode = s.ExamQuestionCode,
                        NameAr = s.NameAr,
                        NameEn = s.NameEn,
                        SortNumber = s.SortNumber,
                        DescriptionAr = s.DescriptionAr,
                        DescriptionEn = s.DescriptionEn,
                        VideoUrl = s.VideoUrl
                    }).OrderBy(x => x.SortNumber).ToList()

                }).OrderBy(x => x.SortNumber).ToList();

            }

            list.Select(c =>
            {
                c.Clauses = c.Question.Count > 0 ? new List<DTOExam>() { new DTOExam() {
                    SortNumber=c.SortNumber,
                    Degree=c.Degree,
                    ExamAge=c.ExamAge,
                    Clauses=c.Clauses,
                    ExamCode=c.ExamCode,
                    Id=c.Id,
                    Name=c.Name,
                    NameAr=c.NameAr,
                    NameEn=c.NameEn,
                    ParenttId=c.ParenttId,
                    Question=c.Question.ToList()} } : c.Clauses; return c;
            }).ToList();
            return list;
        }

        public List<DTOExam> SelectChildPerformanceBaseExamsByAge(int? ExamAge)
        {
            int? currentExamAge = ExamAge - 12;
            var list = new List<DTOExam>();
            list = FindBy(x => x.ExamAge < currentExamAge && x.ParenttId == null).Select(q => new DTOExam
            {
                Id = q.Id,
                ExamCode = q.ExamCode,
                NameEn = q.NameEn,
                NameAr = q.NameAr,
                ExamAge = q.ExamAge,
                Degree = q.Degree,
                ParenttId = q.ParenttId,
                SortNumber = q.SortNumber,


                Question = q.ExamQuestions.Where(x => x.ExamId == q.Id).Select(s => new DTOExamQuestion()
                {
                    Id = s.Id,
                    ExamQuestionCode = s.ExamQuestionCode,
                    NameAr = s.NameAr,
                    NameEn = s.NameEn,
                    SortNumber = s.SortNumber,
                    DescriptionAr = s.DescriptionAr,
                    DescriptionEn = s.DescriptionEn,
                    VideoUrl = s.VideoUrl
                }).OrderBy(x => x.SortNumber).ToList()
            }).OrderBy(x => x.SortNumber).ToList();

            foreach (var item in list)
            {
                item.Clauses = FindBy(x => x.ParenttId != null && x.ParenttId == item.Id).Select(q => new DTOExam
                {
                    Id = q.Id,
                    ExamCode = q.ExamCode,
                    NameEn = q.NameEn,
                    NameAr = q.NameAr,
                    ExamAge = q.ExamAge,
                    Degree = q.Degree,
                    ParenttId = q.ParenttId,
                    SortNumber = q.SortNumber,

                    Question = q.ExamQuestions.Where(x => x.ExamId == q.Id).Select(s => new DTOExamQuestion()
                    {
                        Id = s.Id,
                        ExamQuestionCode = s.ExamQuestionCode,
                        NameAr = s.NameAr,
                        NameEn = s.NameEn,
                        SortNumber = s.SortNumber,
                        DescriptionAr = s.DescriptionAr,
                        DescriptionEn = s.DescriptionEn,
                        VideoUrl = s.VideoUrl
                    }).OrderBy(x => x.SortNumber).ToList()
                }).OrderBy(x => x.SortNumber).ToList();


            }
            list.Select(c =>
            {
                c.Clauses = c.Question.Count > 0 ? new List<DTOExam>() { new DTOExam() {
                    SortNumber=c.SortNumber,
                    Degree=c.Degree,
                    ExamAge=c.ExamAge,
                    Clauses=c.Clauses,
                    ExamCode=c.ExamCode,
                    Id=c.Id,
                    Name=c.Name,
                    NameAr=c.NameAr,
                    NameEn=c.NameEn,
                    ParenttId=c.ParenttId,
                    Question=c.Question.ToList()} } : c.Clauses; return c;
            }).ToList();
            return list;
        }
    }
}

