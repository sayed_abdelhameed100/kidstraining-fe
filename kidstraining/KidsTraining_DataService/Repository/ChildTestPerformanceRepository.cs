using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;


namespace kidsTraining_DataService.Repository
{
    public class ChildTestPerformanceRepository : GenericRepository<ChildTestPerformance>, IChildTestPerformanceRepository
    {

        private IMapper _mapper;
        public ChildTestPerformanceRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOChildTestPerformance AddItem(DTOChildTestPerformance Item)
        {
            var result = _mapper.Map<ChildTestPerformance>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOChildTestPerformance EditItem(DTOChildTestPerformance Item)
        {
            var result = _mapper.Map<ChildTestPerformance>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOChildTestPerformance> Items)
        {
            foreach (DTOChildTestPerformance Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOChildTestPerformance Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOChildTestPerformance> SelectAll()
        {
            var list = new List<DTOChildTestPerformance>();
            list = GetAll().Select(q => new DTOChildTestPerformance
                    {
                                        Id = q.Id, 
                        ChildId = q.ChildId, 
                        ExamStartId = q.ExamStartId, 
                        IsCurrentTest = q.IsCurrentTest, 
                        ChildPerformanceCeilingId = q.ChildPerformanceCeilingId, 
                        ChildPerformanceBaseId = q.ChildPerformanceBaseId, 

                    }).ToList();
            return list;
        }
        public DTOChildTestPerformance SelectById(int id)
        {
            var list = new DTOChildTestPerformance();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOChildTestPerformance
                     {
                                        Id = q.Id, 
                        ChildId = q.ChildId, 
                        ExamStartId = q.ExamStartId, 
                        IsCurrentTest = q.IsCurrentTest, 
                        ChildPerformanceCeilingId = q.ChildPerformanceCeilingId, 
                        ChildPerformanceBaseId = q.ChildPerformanceBaseId, 

                    }).FirstOrDefault();
            return list;
        }

    }
}

