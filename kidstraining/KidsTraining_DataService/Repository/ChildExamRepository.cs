using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;


namespace kidsTraining_DataService.Repository
{
    public class ChildExamRepository : GenericRepository<ChildExam>, IChildExamRepository
    {

        private IMapper _mapper;
        public ChildExamRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {
            _mapper = m;
        }
        public DTOChildExam AddItem(DTOChildExam Item)
        {
            var result = _mapper.Map<ChildExam>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOChildExam EditItem(DTOChildExam Item)
        {
            var result = _mapper.Map<ChildExam>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOChildExam> Items)
        {
            foreach (DTOChildExam Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOChildExam Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOChildExam> SelectAll()
        {
            var list = new List<DTOChildExam>();
            list = GetAll().Select(q => new DTOChildExam
            {
                Id = q.Id,
                ChildId = q.ChildId,
                ExamId = q.ExamId,
                TotalDegree = q.TotalDegree,

            }).ToList();
            return list;
        }
        public DTOChildExam SelectById(int id)
        {
            var list = new DTOChildExam();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOChildExam
                    {
                        Id = q.Id,
                        ChildId = q.ChildId,
                        ExamId = q.ExamId,
                        TotalDegree = q.TotalDegree,

                    }).FirstOrDefault();
            return list;
        }

    }
}

