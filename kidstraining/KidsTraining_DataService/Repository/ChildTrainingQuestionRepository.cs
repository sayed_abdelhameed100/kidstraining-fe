using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;

namespace kidsTraining_DataService.Repository
{
    public class ChildTrainingQuestionRepository : GenericRepository<ChildTrainingQuestion>, IChildTrainingQuestionRepository
    {

        private IMapper _mapper;
        public ChildTrainingQuestionRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOChildTrainingQuestion AddItem(DTOChildTrainingQuestion Item)
        {
            var result = _mapper.Map<ChildTrainingQuestion>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOChildTrainingQuestion EditItem(DTOChildTrainingQuestion Item)
        {
            var result = _mapper.Map<ChildTrainingQuestion>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOChildTrainingQuestion> Items)
        {
            foreach (DTOChildTrainingQuestion Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOChildTrainingQuestion Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOChildTrainingQuestion> SelectAll()
        {
            var list = new List<DTOChildTrainingQuestion>();
            list = GetAll().Select(q => new DTOChildTrainingQuestion
                    {
                                        Id = q.Id, 
                        ChildTrainingId = q.ChildTrainingId, 
                        FinishedTraining = q.FinishedTraining, 
                        ExamQuestionId = q.ExamQuestionId, 

                    }).ToList();
            return list;
        }
        public DTOChildTrainingQuestion SelectById(int id)
        {
            var list = new DTOChildTrainingQuestion();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOChildTrainingQuestion
                     {
                                        Id = q.Id, 
                        ChildTrainingId = q.ChildTrainingId, 
                        FinishedTraining = q.FinishedTraining, 
                        ExamQuestionId = q.ExamQuestionId, 

                    }).FirstOrDefault();
            return list;
        }

    }
}

