using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using KidsTraining_Model.DTOModel;
using Microsoft.AspNetCore.Http;

namespace kidsTraining_DataService.Repository
{
    public class DefaultDataRepository : GenericRepository<DefaultDatum>, IDefaultDataRepository
    {

        private IMapper _mapper;
        private readonly IHttpContextAccessor _accessor;
        private readonly string Lang = "ar";
        public DefaultDataRepository(kidsTrainingDBContext c, IMapper m, IHttpContextAccessor accessor) : base(c)
        {
            _accessor = accessor;
            Lang = _accessor.HttpContext.Request.Headers["Lang"];
            _mapper = m;
        }
        public DTODefaultData AddItem(DTODefaultData Item)
        {
            var result = _mapper.Map<DefaultDatum>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTODefaultData EditItem(DTODefaultData Item)
        {
            var result = _mapper.Map<DefaultDatum>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTODefaultData> Items)
        {
            foreach (DTODefaultData Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTODefaultData Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTODefaultData> SelectAll(int? TypeId)
        {
            var list = new List<DTODefaultData>();
            list = FindBy(x => x.Type == TypeId).Select(q => new DTODefaultData
            {
                Id = q.Id,
                NameEn = q.NameEn,
                NameAr = q.NameAr,
                Type = q.Type,

            }).ToList();
            return list;
        }

        public List<DTODropDown> SelectAllForDropDown(int? TypeId)
        {

            var list = FindBy(x => x.Type == TypeId).Select(q => new DTODropDown
            {
                Value = q.Id,
                Label = Lang == "ar" ? q.NameAr : q.NameEn
            }).ToList();
            return list;
        }
        public DTODefaultData SelectById(int id)
        {
            var list = new DTODefaultData();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTODefaultData
                    {
                        Id = q.Id,
                        NameEn = q.NameEn,
                        NameAr = q.NameAr,
                        Type = q.Type,

                    }).FirstOrDefault();
            return list;
        }

    }
}

