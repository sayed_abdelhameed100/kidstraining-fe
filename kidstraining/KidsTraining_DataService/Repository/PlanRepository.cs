using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using KidsTraining_Model.DTOModel;

namespace kidsTraining_DataService.Repository
{
    public class PlanRepository : GenericRepository<Plan>, IPlanRepository
    {

        private IMapper _mapper;
        public PlanRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOPlan AddItem(DTOPlan Item)
        {
            var result = _mapper.Map<Plan>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOPlan EditItem(DTOPlan Item)
        {
            var result = _mapper.Map<Plan>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOPlan> Items)
        {
            foreach (DTOPlan Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOPlan Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOPlan> SelectAll()
        {
            var list = new List<DTOPlan>();
            list = FindBy(x => x.IsPay == false).Select(q => new DTOPlan
            {
                Id = q.Id,
                NameEn = q.NameEn,
                NameAr = q.NameAr,
                Cost = q.Cost,
                IsPay = q.IsPay,
                TimePerMonth = q.TimePerMonth,

            }).ToList();
            return list;
        }
        public DTOPlan SelectById(int id)
        {
            var list = new DTOPlan();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOPlan
                    {
                        Id = q.Id,
                        NameEn = q.NameEn,
                        NameAr = q.NameAr,
                        Cost = q.Cost,
                        IsPay = q.IsPay,
                        TimePerMonth = q.TimePerMonth,

                    }).FirstOrDefault();
            return list;
        }

        public List<DTODropDown> GetPlansForDropDown()
        {

            var list = FindBy(x => x.IsPay == true)
                    .Select(q => new DTODropDown
                    {
                        Value = q.Id,
                        Label = q.NameAr,
                    }).ToList();
            return list;
        }

    }
}

