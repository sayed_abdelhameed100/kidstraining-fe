using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;

namespace kidsTraining_DataService.Repository
{
    public class UserPlanRepository : GenericRepository<UserPlan>, IUserPlanRepository
    {

        private IMapper _mapper;
        private readonly IHttpContextAccessor _accessor;
        private readonly string Lang = "ar";
        private readonly int? UserId = null;
        public UserPlanRepository(kidsTrainingDBContext c, IMapper m, IHttpContextAccessor accessor) : base(c)
        {
            _mapper = m;
            _accessor = accessor;
            Lang = _accessor.HttpContext.Request.Headers["Lang"];
        }
        public DTOUserPlan AddItem(DTOUserPlan Item)
        {
            int userId = Convert.ToInt32(_accessor.HttpContext.User.FindFirst(c => c.Type == "userId").Value);
            var plan = Context.Plans.Find(Item.PlanId);
            this.UpdateIsCurrentPlan(userId);
            Item.UserId = userId;
            Item.IsCurrentPlan = true;

            Item.EndDate = Item.StartDate.Value.AddMonths((int)plan.TimePerMonth);
            Item.Total = plan.Cost;
            // Item.PaymentDate = DateTime.Now;
            // Item.PaymentRefNum = "";
            // Item.PaymentStatus = true;

            var result = _mapper.Map<UserPlan>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOUserPlan EditItem(DTOUserPlan Item)
        {
            var result = _mapper.Map<UserPlan>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOUserPlan> Items)
        {
            foreach (DTOUserPlan Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOUserPlan Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOUserPlan> SelectAll()
        {
            var list = new List<DTOUserPlan>();
            list = GetAll().Select(q => new DTOUserPlan
            {
                Id = q.Id,
                UserId = q.UserId,
                PlanId = q.PlanId,
                StartDate = q.StartDate,
                EndDate = q.EndDate,
                IsCurrentPlan = q.IsCurrentPlan,
                PaymentStatus = q.PaymentStatus,
                PaymentRefNum = q.PaymentRefNum,
                PaymentDate = q.PaymentDate,
                Total = q.Total
            }).ToList();
            return list;
        }
        public DTOUserPlan SelectById(int id)
        {
            var list = new DTOUserPlan();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOUserPlan
                    {
                        Id = q.Id,
                        UserId = q.UserId,
                        PlanId = q.PlanId,
                        StartDate = q.StartDate,
                        EndDate = q.EndDate,
                        IsCurrentPlan = q.IsCurrentPlan,
                        PaymentStatus = q.PaymentStatus,
                        PaymentRefNum = q.PaymentRefNum,
                        PaymentDate = q.PaymentDate,
                        Total = q.Total
                    }).FirstOrDefault();
            return list;
        }


        private void UpdateIsCurrentPlan(int UserId)
        {
            var Items = FindBy(x => x.UserId == UserId).ToList();
            foreach (var Item in Items)
            {
                Item.IsCurrentPlan = false;
                Edit(Item);
            }
            Save();
        }

    }
}

