using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;


namespace kidsTraining_DataService.Repository
{
    public class ChildTrainingRepository : GenericRepository<ChildTraining>, IChildTrainingRepository
    {

        private IMapper _mapper;
        public ChildTrainingRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOChildTraining AddItem(DTOChildTraining Item)
        {
            var result = _mapper.Map<ChildTraining>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOChildTraining EditItem(DTOChildTraining Item)
        {
            var result = _mapper.Map<ChildTraining>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOChildTraining> Items)
        {
            foreach (DTOChildTraining Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOChildTraining Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOChildTraining> SelectAll()
        {
            var list = new List<DTOChildTraining>();
            list = GetAll().Select(q => new DTOChildTraining
                    {
                                        Id = q.Id, 
                        ChildId = q.ChildId, 
                        ExamId = q.ExamId, 
                        FinishedTraining = q.FinishedTraining, 

                    }).ToList();
            return list;
        }
        public DTOChildTraining SelectById(int id)
        {
            var list = new DTOChildTraining();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOChildTraining
                     {
                                        Id = q.Id, 
                        ChildId = q.ChildId, 
                        ExamId = q.ExamId, 
                        FinishedTraining = q.FinishedTraining, 

                    }).FirstOrDefault();
            return list;
        }

    }
}

