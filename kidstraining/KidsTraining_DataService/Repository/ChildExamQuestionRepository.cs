using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using KidsTraining_DataService.CommandService;

namespace kidsTraining_DataService.Repository
{
    public class ChildExamQuestionRepository : GenericRepository<ChildExamQuestion>, IChildExamQuestionRepository
    {

        private IMapper _mapper;
        public ChildExamQuestionRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOChildExamQuestion AddItem(DTOChildExamQuestion Item)
        {
            var result = _mapper.Map<ChildExamQuestion>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOChildExamQuestion EditItem(DTOChildExamQuestion Item)
        {
            var result = _mapper.Map<ChildExamQuestion>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOChildExamQuestion> Items)
        {
            foreach (DTOChildExamQuestion Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOChildExamQuestion Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOChildExamQuestion> SelectAll()
        {
            var list = new List<DTOChildExamQuestion>();
            list = GetAll().Select(q => new DTOChildExamQuestion
                    {
                                        Id = q.Id, 
                        ChildExamId = q.ChildExamId, 
                        ExamQuestionId = q.ExamQuestionId, 
                        Degree = q.Degree, 

                    }).ToList();
            return list;
        }
        public DTOChildExamQuestion SelectById(int id)
        {
            var list = new DTOChildExamQuestion();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOChildExamQuestion
                     {
                                        Id = q.Id, 
                        ChildExamId = q.ChildExamId, 
                        ExamQuestionId = q.ExamQuestionId, 
                        Degree = q.Degree, 

                    }).FirstOrDefault();
            return list;
        }

    }
}

