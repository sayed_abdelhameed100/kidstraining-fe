using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;


namespace kidsTraining_DataService.Repository
{
    public class ExamQuestionRepository : GenericRepository<ExamQuestion>, IExamQuestionRepository
    {

        private IMapper _mapper;
        public ExamQuestionRepository(kidsTrainingDBContext c, IMapper m) : base(c)
        {

            _mapper = m;
        }
        public DTOExamQuestion AddItem(DTOExamQuestion Item)
        {
            var result = _mapper.Map<ExamQuestion>(Item);
            Add(result);
            Save();
            Item.Id = result.Id;
            return Item;

        }
        public DTOExamQuestion EditItem(DTOExamQuestion Item)
        {
            var result = _mapper.Map<ExamQuestion>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOExamQuestion> Items)
        {
            foreach (DTOExamQuestion Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOExamQuestion Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }

        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOExamQuestion> SelectAll()
        {
            var list = new List<DTOExamQuestion>();
            list = GetAll().Select(q => new DTOExamQuestion
                    {
                                        Id = q.Id, 
                        ExamQuestionCode = q.ExamQuestionCode, 
                        NameEn = q.NameEn, 
                        NameAr = q.NameAr, 
                        ExamId = q.ExamId, 
                        SortNumber = q.SortNumber, 
                        VideoUrl = q.VideoUrl, 
                        DescriptionAr = q.DescriptionAr, 
                        DescriptionEn = q.DescriptionEn, 

                    }).ToList();
            return list;
        }
        public DTOExamQuestion SelectById(int id)
        {
            var list = new DTOExamQuestion();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOExamQuestion
                     {
                                        Id = q.Id, 
                        ExamQuestionCode = q.ExamQuestionCode, 
                        NameEn = q.NameEn, 
                        NameAr = q.NameAr, 
                        ExamId = q.ExamId, 
                        SortNumber = q.SortNumber, 
                        VideoUrl = q.VideoUrl, 
                        DescriptionAr = q.DescriptionAr, 
                        DescriptionEn = q.DescriptionEn, 

                    }).FirstOrDefault();
            return list;
        }

    }
}

