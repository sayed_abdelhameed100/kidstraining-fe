using KidsTraining_Context.DBContext;
using KidsTraining_DataService.CommandService;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using KidsTraining_Model.Settings;
using Microsoft.Extensions.Options;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Net;
using Microsoft.EntityFrameworkCore;

namespace kidsTraining_DataService.Repository
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {

        private IMapper _mapper;
        private readonly DTOConfig _appSettings;
        private readonly IHttpContextAccessor _accessor;
        private readonly string Lang = "ar";
        private readonly int? UserId = null;
        public UserRepository(kidsTrainingDBContext c, IMapper m, IOptions<DTOConfig> appSettings, IHttpContextAccessor accessor) : base(c)
        {
            _appSettings = appSettings.Value;
            _mapper = m;
            _accessor = accessor;
            Lang = _accessor.HttpContext.Request.Headers["Lang"];
        }
        public DTOUser AddItem(DTOUser Item)
        {

            var result = _mapper.Map<User>(Item);
            result.CreateOn = DateTime.Now;
            result.IsActice = true;
            result.OtpCode = this.CreateOtp(result.Phone);
            result.IsVerifyOtp = false;
            result.OtpDate = DateTime.Now;

            Add(result);
            Save();
            var userPlan = new UserPlan() { PlanId = 1, IsCurrentPlan = true, UserId = result.Id, Total = 0, StartDate = DateTime.Now, EndDate = DateTime.Now.AddDays(30) };
            Context.UserPlans.Add(userPlan);
            Context.SaveChanges();

            Item.Id = result.Id;
            return Item;

        }
        public DTOUser EditItem(DTOUser Item)
        {
            var result = _mapper.Map<User>(Item);
            Edit(result);
            Save();
            return Item;
        }
        public void DeleteItems(List<DTOUser> Items)
        {
            foreach (DTOUser Item in Items)
            {
                var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(DTOUser Item)
        {
            var i = FindBy(x => x.Id == Item.Id).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public void DeleteItems(List<int> ItemIDs)
        {
            foreach (int ItemId in ItemIDs)
            {
                var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
                if (i != null)
                {
                    Delete(i);
                    Save();
                }
            }
        }
        public void DeleteItems(int ItemId)
        {
            var i = FindBy(x => x.Id == ItemId).FirstOrDefault();
            if (i != null)
            {
                Delete(i);
                Save();
            }
        }
        public List<DTOUser> SelectAll()
        {
            var list = new List<DTOUser>();
            list = GetAll().Select(q => new DTOUser
            {
                Id = q.Id,
                UserName = q.UserName,
                NameAr = q.NameAr,
                NameEn = q.NameEn,
                Email = q.Email,
                Phone = q.Phone,
                Password = q.Password,
                Address = q.Address,
                CreateOn = q.CreateOn,
                IsActice = q.IsActice,
                LogoUrl = q.LogoUrl,

            }).ToList();
            return list;
        }
        public DTOUser SelectById(int id)
        {
            var list = new DTOUser();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOUser
                    {
                        Id = q.Id,
                        UserName = q.UserName,
                        NameAr = q.NameAr,
                        NameEn = q.NameEn,
                        Email = q.Email,
                        Phone = q.Phone,
                        Password = q.Password,
                        Address = q.Address,
                        CreateOn = q.CreateOn,
                        IsActice = q.IsActice,
                        LogoUrl = q.LogoUrl,
                        CurrentUserPlan = q.UserPlans.Where(x => x.IsCurrentPlan == true).Select(x => new DTOUserPlan()
                        {
                            PaymentDate = x.PaymentDate,
                            StartDate = x.StartDate,
                            PaymentRefNum = x.PaymentRefNum,
                            EndDate = x.EndDate,
                            PlanName = x.Plan.NameAr
                        }).FirstOrDefault(),

                        UserPlan = q.UserPlans.Select(x => new DTOUserPlan()
                        {
                            PaymentDate = x.PaymentDate,
                            StartDate = x.StartDate,
                            PaymentRefNum = x.PaymentRefNum,
                            EndDate = x.EndDate,
                            PlanName = x.Plan.NameAr
                        }).ToList(),

                    }).FirstOrDefault();
            return list;
        }

        public int CheckUser(string UserName)
        {
            var user = FindBy(x => x.UserName.ToLower() == UserName.ToLower()).AsNoTracking().FirstOrDefault();

            if (user == null)
                return 0;
            else
                return user.Id;
        }

        public bool ForGetPasswaord(int Id, string Password)
        {
            var item = Context.Users.Find(Id);
            if (item == null)
            {
                return false;
            }
            item.Password = Password;
            Edit(item);
            Save();
            return true;
        }

        private string GetToken(DTOUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.SecurityKey);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.ToString()),
                     new Claim("userId", user.Id.ToString()),
                     //new Claim("logo",user.LogoUrl.ToString()),
                     new Claim("userName",user.NameAr),
                     new Claim("fullUserData",user.ToString()),
                     new Claim("expairDate", user.CurrentUserPlan != null ?user.CurrentUserPlan.EndDate.Value.Date.ToString():"")
                }),
                Expires = DateTime.UtcNow.AddHours(6),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public DTOUser Authenticate(string Name, string Password)
        {
            var user = new DTOUser();
            int index = Name.IndexOf('@');
            if (index == -1)
            {
                user = (from q in Context.Users
                        where q.UserName == Name && q.Password == Password  
                        select new DTOUser
                        {
                            Id = q.Id,
                            UserName = q.UserName,
                            Email = q.Email,
                            Phone = q.Phone,
                            LogoUrl = q.LogoUrl,
                            NameAr = q.NameAr,
                            NameEn = q.NameEn,
                            IsActice=q.IsActice,
                            IsVerifyOtp=q.IsVerifyOtp,
                            CurrentUserPlan = q.UserPlans.Where(x => x.IsCurrentPlan == true).Select(x => new DTOUserPlan()
                            {
                                PaymentDate = x.PaymentDate,
                                StartDate = x.StartDate,
                                PaymentRefNum = x.PaymentRefNum,
                                EndDate = x.EndDate,
                                PlanName = x.Plan.NameAr
                            }).FirstOrDefault(),
                        }).FirstOrDefault();
            }
            else
            {
                user = (from q in Context.Users
                        where q.Email == Name && q.Password == Password 
                        select new DTOUser
                        {
                            Id = q.Id,
                            UserName = q.UserName,
                            Email = q.Email,
                            Phone = q.Phone,
                            LogoUrl = q.LogoUrl,
                            NameAr = q.NameAr,
                            NameEn = q.NameEn,
                            IsActice = q.IsActice,
                            IsVerifyOtp = q.IsVerifyOtp,
                            CurrentUserPlan = q.UserPlans.Where(x => x.IsCurrentPlan == true).Select(x => new DTOUserPlan()
                            {
                                PaymentDate = x.PaymentDate,
                                StartDate = x.StartDate,
                                PaymentRefNum = x.PaymentRefNum,
                                EndDate = x.EndDate,
                                PlanName = x.Plan.NameAr
                            }).FirstOrDefault(),
                        }).FirstOrDefault();
            }

            if (user == null)
                return null;

            user.Token = this.GetToken(user);
            return user;
        }

        public int CheckUserNumber(string phone)
        {
            var user = FindBy(x => x.Phone.Substring(1, x.Phone.Length - 1) == phone).AsNoTracking().FirstOrDefault();

            if (user == null)
                return 0;
            else
                return user.Id;
        }

        public int CheckUserEmail(string Email)
        {
            var user = FindBy(x => x.Email.ToLower() == Email.ToLower()).AsNoTracking().FirstOrDefault();

            if (user == null)
                return 0;
            else
                return user.Id;
        }

        public DTOUser GetCurrentUserInfo()
        {
            int id = Convert.ToInt32(_accessor.HttpContext.User.FindFirst(c => c.Type == "userId").Value);
            var list = new DTOUser();
            list = FindBy(x => x.Id == id)
                    .Select(q => new DTOUser
                    {
                        Id = q.Id,
                        UserName = q.UserName,
                        NameAr = q.NameAr,
                        NameEn = q.NameEn,
                        Email = q.Email,
                        Phone = q.Phone,
                        Password = q.Password,
                        Address = q.Address,
                        CreateOn = q.CreateOn,
                        IsActice = q.IsActice,
                        LogoUrl = q.LogoUrl,
                        CurrentUserPlan = q.UserPlans.Where(x => x.IsCurrentPlan == true).Select(x => new DTOUserPlan()
                        {
                            Id = x.Id,
                            PaymentDate = x.PaymentDate,
                            StartDate = x.StartDate,
                            PaymentRefNum = x.PaymentRefNum,
                            EndDate = x.EndDate,
                            PlanName = x.Plan.NameAr,
                            IsCurrentPlan = x.IsCurrentPlan
                        }).FirstOrDefault(),

                        UserPlan = q.UserPlans.Select(x => new DTOUserPlan()
                        {
                            Id = x.Id,
                            PaymentDate = x.PaymentDate,
                            StartDate = x.StartDate,
                            PaymentRefNum = x.PaymentRefNum,
                            EndDate = x.EndDate,
                            PlanName = x.Plan.NameAr,
                            IsCurrentPlan = x.IsCurrentPlan
                        }).ToList(),

                    }).FirstOrDefault();
            return list;
        }

        private int CreateOtp(string Phone)
        {
            int otp = new Random().Next(1000, 9999);
            string msg = "your otp for dr sherbiny site is :" + otp;
            //this.SendingSMS(msg, Phone);
            return otp;
        }

        private void SendingSMS(string smsText, string Phone)
        {
            var apiKey = "Pe1Gf1XudFx6FflxWelatpCsc";
            string baseURL = "https://api.mpp-sms.com/api/send.aspx?apikey=" + apiKey + "&language=1&sender=Media Phone&mobile=" + Phone + "&message=" + smsText;

            HttpClient client = new HttpClient();

            System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                HttpResponseMessage messge = client.GetAsync(baseURL).Result;
                if (messge.IsSuccessStatusCode)
                {
                    string result2 = messge.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                    JObject jsonResult = JObject.Parse(result2);
                }
                else
                {
                    //string result2 = messge.Content.ReadAsStringAsync().GetAwaiter().GetResult();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void SendOtp(string Phone)
        {
            var user = FindBy(x => x.Phone.ToLower() == Phone.ToLower()).FirstOrDefault();
            user.OtpCode = this.CreateOtp(Phone);
            user.OtpDate = DateTime.Now;
            user.IsVerifyOtp = false;
            Edit(user);
            Save();
        }

        public bool CheckOtp(int UserId, int? Otp)
        {
            var user = FindBy(x => x.Id == UserId && x.OtpCode == Otp).FirstOrDefault();

            if (user == null)
                return false;

            if (user.OtpDate.Value.AddMinutes(2) > DateTime.Now)
            {
                user.IsVerifyOtp = true;
                Edit(user);
                Save();
                return true;
            }
            else
            {
                return false;
            }


        }
    }
}

