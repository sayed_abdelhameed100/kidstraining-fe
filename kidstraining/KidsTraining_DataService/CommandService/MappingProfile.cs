﻿using AutoMapper;
using KidsTraining_Context.DBContext;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KidsTraining_DataService.CommandService
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<Child, DTOChildren>().ReverseMap();
            CreateMap<Exam, DTOExam>().ReverseMap();
            CreateMap<UserPlan, DTOUserPlan>().ReverseMap();
            CreateMap<ChildExam, DTOChildExam>().ReverseMap();
            CreateMap<ChildExamQuestion, DTOChildExamQuestion>().ReverseMap();
            CreateMap<ChildTraining, DTOChildTraining>().ReverseMap();
            CreateMap<ChildTrainingQuestion, DTOChildTrainingQuestion>().ReverseMap();
            CreateMap<DefaultDatum, DTODefaultData>().ReverseMap();
            CreateMap<Plan, DTOPlan>().ReverseMap();
            CreateMap<ExamQuestion, DTOExamQuestion>().ReverseMap();
            CreateMap<User, DTOUser>().ReverseMap();
            CreateMap<ChildTestPerformance, DTOChildTestPerformance>().ReverseMap();
        }
    }
}
