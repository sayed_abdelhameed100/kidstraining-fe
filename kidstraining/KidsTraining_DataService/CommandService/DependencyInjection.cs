﻿using kidsTraining_DataService.Repository;
using kidsTraining_Interface.IDataService;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KidsTraining_DataService.CommandService
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddRepository(this IServiceCollection services)
        {
            services.AddScoped<IChildrenRepository, ChildrenRepository>();
            services.AddScoped<IExamRepository, ExamRepository>();
            services.AddScoped<IUserPlanRepository, UserPlanRepository>();
            services.AddScoped<IChildExamRepository, ChildExamRepository>();
            services.AddScoped<IChildExamQuestionRepository, ChildExamQuestionRepository>();
            services.AddScoped<IChildTrainingRepository, ChildTrainingRepository>();
            services.AddScoped<IChildTrainingQuestionRepository, ChildTrainingQuestionRepository>();
            services.AddScoped<IDefaultDataRepository, DefaultDataRepository>();
            services.AddScoped<IPlanRepository, PlanRepository>();
            services.AddScoped<IExamQuestionRepository, ExamQuestionRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IChildTestPerformanceRepository, ChildTestPerformanceRepository>();
            return services;
        }
    }
}
