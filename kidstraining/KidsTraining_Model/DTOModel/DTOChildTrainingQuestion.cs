using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOChildTrainingQuestion
    {
        public int Id { get; set; } 
        public int? ChildTrainingId { get; set; } 
        public bool? FinishedTraining { get; set; } 
        public int? ExamQuestionId { get; set; } 

    }
}
