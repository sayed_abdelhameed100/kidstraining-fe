using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTODefaultData
    {
        public int Id { get; set; } 
        public string NameEn { get; set; } 
        public string Name { get; set; } 
        public string NameAr { get; set; } 
        public int? Type { get; set; } 

    }
}
