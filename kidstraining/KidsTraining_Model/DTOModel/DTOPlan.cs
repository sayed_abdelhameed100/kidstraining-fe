using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOPlan
    {
        public int Id { get; set; } 
        public string NameEn { get; set; } 
        public string Name { get; set; } 
        public string NameAr { get; set; } 
        public double? Cost { get; set; } 
        public bool? IsPay { get; set; } 
        public int? TimePerMonth { get; set; } 

    }
}
