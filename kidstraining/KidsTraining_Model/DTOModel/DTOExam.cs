using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOExam
    {
        public int Id { get; set; }
        public int? ExamCode { get; set; }
        public string NameEn { get; set; }
        public string Name { get; set; }
        public string NameAr { get; set; }
        public int? ExamAge { get; set; }
        public int? Degree { get; set; }
        public int? ParenttId { get; set; }
        public int? SortNumber { get; set; }
        public List<DTOExamQuestion> Question { get; set; }
        public List<DTOExam> Clauses { get; set; }

    }

}
