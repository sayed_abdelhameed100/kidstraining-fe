using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOChildExam
    {
        public int Id { get; set; } 
        public int? ChildId { get; set; } 
        public int? ExamId { get; set; } 
        public int? TotalDegree { get; set; } 

    }
}
