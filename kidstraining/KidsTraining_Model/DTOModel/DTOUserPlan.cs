using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOUserPlan
    {
        public int Id { get; set; } 
        public int? UserId { get; set; } 
        public int? PlanId { get; set; } 
        public DateTime? StartDate { get; set; } 
        public DateTime? EndDate { get; set; } 
        public bool? IsCurrentPlan { get; set; } 
        public bool? PaymentStatus { get; set; } 
        public long? PaymentRefNum { get; set; } 
        public DateTime? PaymentDate { get; set; } 
        public double? Total { get; set; }
        public string PlanName { get; set; }

    }
}
