using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOChildTestPerformance
    {
        public int Id { get; set; } 
        public int? ChildId { get; set; } 
        public bool? IsDescendingTest { get; set; } 
        public int? ExamStartId { get; set; } 
        public bool? IsCurrentTest { get; set; } 
        public int? ChildPerformanceCeilingId { get; set; } 
        public int? ChildPerformanceBaseId { get; set; } 

    }
}
