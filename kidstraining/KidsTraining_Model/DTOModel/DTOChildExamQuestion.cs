using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOChildExamQuestion
    {
        public int Id { get; set; } 
        public int? ChildExamId { get; set; } 
        public int? ExamQuestionId { get; set; } 
        public int? Degree { get; set; } 

    }
}
