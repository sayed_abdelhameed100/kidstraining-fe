using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOChildren
    {
        public int Id { get; set; } 
        public string Name { get; set; } 
        public string ChildLang { get; set; } 
        public int? Sex { get; set; } 
        public string Diagnosis { get; set; } 
        public DateTime? Birthdate { get; set; } 
        public int? AgeInMonth { get; set; }

        public int? UserId { get; set; }

        public int? KeyWordLengthId { get; set; }
    }
}
