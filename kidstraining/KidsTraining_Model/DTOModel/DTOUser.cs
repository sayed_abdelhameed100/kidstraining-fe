using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOUser
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public DateTime? CreateOn { get; set; }
        public bool? IsActice { get; set; }
        public string LogoUrl { get; set; }
        public string Token { get; set; }

        public int? OtpCode { get; set; }
        public DateTime? OtpDate { get; set; }
        public bool? IsVerifyOtp { get; set; }
        public List<DTOUserPlan> UserPlan { get; set; }
        public DTOUserPlan CurrentUserPlan { get; set; }


    }
}
