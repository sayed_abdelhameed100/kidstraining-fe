using System;
using System.Collections.Generic;


namespace kidsTraining_Model.DTOModel
{
    public partial class DTOExamQuestion
    {
        public int Id { get; set; } 
        public int? ExamQuestionCode { get; set; } 
        public string NameEn { get; set; } 
        public string Name { get; set; } 
        public string NameAr { get; set; } 
        public int? ExamId { get; set; } 
        public int? SortNumber { get; set; } 
        public string VideoUrl { get; set; } 
        public string DescriptionAr { get; set; } 
        public string DescriptionEn { get; set; } 
        public string Description { get; set; } 

    }
}
