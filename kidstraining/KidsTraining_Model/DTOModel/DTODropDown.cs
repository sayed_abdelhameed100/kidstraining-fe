﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KidsTraining_Model.DTOModel
{
   public class DTODropDown
    {
        public int Value { get; set; }
        public string Label { get; set; }
    }
}
