using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IExamQuestionRepository : IGenericRepository<ExamQuestion>
    {
        DTOExamQuestion AddItem(DTOExamQuestion Item);
        DTOExamQuestion EditItem(DTOExamQuestion Item);
        void DeleteItems(List<DTOExamQuestion> Items);
        void DeleteItems(DTOExamQuestion Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOExamQuestion> SelectAll();
        DTOExamQuestion SelectById(int id);
    }
}
