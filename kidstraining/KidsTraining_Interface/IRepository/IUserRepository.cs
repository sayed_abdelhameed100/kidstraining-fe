using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IUserRepository : IGenericRepository<User>
    {
        DTOUser AddItem(DTOUser Item);
        DTOUser EditItem(DTOUser Item);
        void DeleteItems(List<DTOUser> Items);
        void DeleteItems(DTOUser Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOUser> SelectAll();
        DTOUser SelectById(int id);
        DTOUser Authenticate(string Name, string Password);
        int CheckUser(string UserName);

        int CheckUserEmail(string Email);
        int CheckUserNumber(string phone);
        bool ForGetPasswaord(int Id, string Password);
        DTOUser GetCurrentUserInfo();
        void SendOtp(string Phone);
        bool CheckOtp(int UserId, int? Otp);
    }
}
