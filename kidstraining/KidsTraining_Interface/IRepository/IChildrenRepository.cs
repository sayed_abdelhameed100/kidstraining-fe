using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IChildrenRepository : IGenericRepository<Child>
    {
        DTOChildren AddItem(DTOChildren Item);
        DTOChildren EditItem(DTOChildren Item);
        void DeleteItems(List<DTOChildren> Items);
        void DeleteItems(DTOChildren Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOChildren> SelectAll();
        DTOChildren SelectById(int id);
    }
}
