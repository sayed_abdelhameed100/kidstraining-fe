using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IChildExamQuestionRepository : IGenericRepository<ChildExamQuestion>
    {
        DTOChildExamQuestion AddItem(DTOChildExamQuestion Item);
        DTOChildExamQuestion EditItem(DTOChildExamQuestion Item);
        void DeleteItems(List<DTOChildExamQuestion> Items);
        void DeleteItems(DTOChildExamQuestion Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOChildExamQuestion> SelectAll();
        DTOChildExamQuestion SelectById(int id);
    }
}
