using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IChildExamRepository : IGenericRepository<ChildExam>
    {
        DTOChildExam AddItem(DTOChildExam Item);
        DTOChildExam EditItem(DTOChildExam Item);
        void DeleteItems(List<DTOChildExam> Items);
        void DeleteItems(DTOChildExam Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOChildExam> SelectAll();
        DTOChildExam SelectById(int id);
    }
}
