using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IChildTrainingQuestionRepository : IGenericRepository<ChildTrainingQuestion>
    {
        DTOChildTrainingQuestion AddItem(DTOChildTrainingQuestion Item);
        DTOChildTrainingQuestion EditItem(DTOChildTrainingQuestion Item);
        void DeleteItems(List<DTOChildTrainingQuestion> Items);
        void DeleteItems(DTOChildTrainingQuestion Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOChildTrainingQuestion> SelectAll();
        DTOChildTrainingQuestion SelectById(int id);
    }
}
