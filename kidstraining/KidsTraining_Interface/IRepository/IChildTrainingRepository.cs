using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IChildTrainingRepository : IGenericRepository<ChildTraining>
    {
        DTOChildTraining AddItem(DTOChildTraining Item);
        DTOChildTraining EditItem(DTOChildTraining Item);
        void DeleteItems(List<DTOChildTraining> Items);
        void DeleteItems(DTOChildTraining Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOChildTraining> SelectAll();
        DTOChildTraining SelectById(int id);
    }
}
