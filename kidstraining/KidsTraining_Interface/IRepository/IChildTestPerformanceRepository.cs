
using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IChildTestPerformanceRepository : IGenericRepository<ChildTestPerformance>
    {
        DTOChildTestPerformance AddItem(DTOChildTestPerformance Item);
        DTOChildTestPerformance EditItem(DTOChildTestPerformance Item);
        void DeleteItems(List<DTOChildTestPerformance> Items);
        void DeleteItems(DTOChildTestPerformance Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOChildTestPerformance> SelectAll();
        DTOChildTestPerformance SelectById(int id);
    }
}
