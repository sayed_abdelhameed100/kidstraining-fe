using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IExamRepository : IGenericRepository<Exam>
    {
        DTOExam AddItem(DTOExam Item);
        DTOExam EditItem(DTOExam Item);
        void DeleteItems(List<DTOExam> Items);
        void DeleteItems(DTOExam Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOExam> SelectAll();
        DTOExam SelectById(int id);
        List<DTOExam> SelectExamsByAge(int? ExamAge);
        int? FirstChildTest(int? ExamAge);

        List<DTOExam> SelectChildPerformanceCeilingExamsByAge(int? ExamAge);
        List<DTOExam> SelectChildPerformanceBaseExamsByAge(int? ExamAge);
    }
}
