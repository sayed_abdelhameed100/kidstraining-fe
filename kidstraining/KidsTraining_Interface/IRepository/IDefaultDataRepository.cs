using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using KidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IDefaultDataRepository : IGenericRepository<DefaultDatum>
    {
        DTODefaultData AddItem(DTODefaultData Item);
        DTODefaultData EditItem(DTODefaultData Item);
        void DeleteItems(List<DTODefaultData> Items);
        void DeleteItems(DTODefaultData Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTODefaultData> SelectAll(int? TypeId);

        List<DTODropDown> SelectAllForDropDown(int? TypeId);
        DTODefaultData SelectById(int id);
    }
}
