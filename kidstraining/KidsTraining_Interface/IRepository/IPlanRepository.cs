using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using KidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IPlanRepository : IGenericRepository<Plan>
    {
        DTOPlan AddItem(DTOPlan Item);
        DTOPlan EditItem(DTOPlan Item);
        void DeleteItems(List<DTOPlan> Items);
        void DeleteItems(DTOPlan Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOPlan> SelectAll();
        DTOPlan SelectById(int id);
        List<DTODropDown> GetPlansForDropDown();
    }
}
