using KidsTraining_Context.DBContext;
using KidsTraining_Interface.IRepository;
using kidsTraining_Model.DTOModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace kidsTraining_Interface.IDataService
{
    public interface IUserPlanRepository : IGenericRepository<UserPlan>
    {
        DTOUserPlan AddItem(DTOUserPlan Item);
        DTOUserPlan EditItem(DTOUserPlan Item);
        void DeleteItems(List<DTOUserPlan> Items);
        void DeleteItems(DTOUserPlan Items);
        void DeleteItems(List<int> ItemIDs);
        void DeleteItems(int itemId);
        List<DTOUserPlan> SelectAll();
        DTOUserPlan SelectById(int id);
    }
}
