using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class ChildTrainingQuestionController : Controller
    {
        private readonly IChildTrainingQuestionRepository _ChildTrainingQuestionRepository;
        public ChildTrainingQuestionController(IChildTrainingQuestionRepository _ChildTrainingQuestion)
        {
            _ChildTrainingQuestionRepository = _ChildTrainingQuestion;
        }

        [Route("~/api/GetAllChildTrainingQuestions")]
        [HttpGet]
        public IActionResult GetAllChildTrainingQuestions()
        {
            var result = _ChildTrainingQuestionRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetChildTrainingQuestionById")]
        [HttpGet]
        public IActionResult GetChildTrainingQuestionById(int id)
        {
            var result = _ChildTrainingQuestionRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditChildTrainingQuestion")]
        [HttpPost]
        public IActionResult EditChildTrainingQuestion([FromBody] DTOChildTrainingQuestion Item)
        {
            var _Item = _ChildTrainingQuestionRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddChildTrainingQuestion")]
        [HttpPost]
        public IActionResult AddChildTrainingQuestion([FromBody] DTOChildTrainingQuestion Item)
        {
            var _Item = _ChildTrainingQuestionRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteChildTrainingQuestionsByList")]
        [HttpPost]
        public IActionResult DeleteChildTrainingQuestionsByList([FromBody] List<DTOChildTrainingQuestion> Items)
        {
            _ChildTrainingQuestionRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteChildTrainingQuestionByItem")]
        [HttpPost]
        public IActionResult DeleteChildTrainingQuestionByItem([FromBody] DTOChildTrainingQuestion Item)
        {
            _ChildTrainingQuestionRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteChildTrainingQuestionsByIds")]
        [HttpPost]
        public IActionResult DeleteChildTrainingQuestionsByIds([FromBody] List<int> ItemIds)
        {
            _ChildTrainingQuestionRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteChildTrainingQuestionById")]
        [HttpPost]
        public IActionResult DeleteChildTrainingQuestionById([FromBody] int ItemId)
        {
            _ChildTrainingQuestionRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
