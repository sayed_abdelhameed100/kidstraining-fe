using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class ChildrenController : Controller
    {
        private readonly IChildrenRepository _ChildrenRepository;
        public ChildrenController(IChildrenRepository _Children)
        {
            _ChildrenRepository = _Children;
        }

        [Route("~/api/GetAllChildrens")]
        [HttpGet]
        public IActionResult GetAllChildrens()
        {
            var result = _ChildrenRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetChildrenById")]
        [HttpGet]
        public IActionResult GetChildrenById(int id)
        {
            var result = _ChildrenRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditChildren")]
        [HttpPost]
        public IActionResult EditChildren([FromBody] DTOChildren Item)
        {
            var _Item = _ChildrenRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddChildren")]
        [HttpPost]
        public IActionResult AddChildren([FromBody] DTOChildren Item)
        {
            var _Item = _ChildrenRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteChildrensByList")]
        [HttpPost]
        public IActionResult DeleteChildrensByList([FromBody] List<DTOChildren> Items)
        {
            _ChildrenRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteChildrenByItem")]
        [HttpPost]
        public IActionResult DeleteChildrenByItem([FromBody] DTOChildren Item)
        {
            _ChildrenRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteChildrensByIds")]
        [HttpPost]
        public IActionResult DeleteChildrensByIds([FromBody] List<int> ItemIds)
        {
            _ChildrenRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteChildrenById")]
        [HttpPost]
        public IActionResult DeleteChildrenById([FromBody] int ItemId)
        {
            _ChildrenRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
