using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("CorsPolicy")]
    public class ChildTestPerformanceController : Controller
    {
        private readonly IChildTestPerformanceRepository _ChildTestPerformanceRepository;
        public ChildTestPerformanceController(IChildTestPerformanceRepository _ChildTestPerformance)
        {
            _ChildTestPerformanceRepository = _ChildTestPerformance;
        }

        [Route("~/api/GetAllChildTestPerformances")]
        [HttpGet]
        public IActionResult GetAllChildTestPerformances()
        {
            var result = _ChildTestPerformanceRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetChildTestPerformanceById")]
        [HttpGet]
        public IActionResult GetChildTestPerformanceById(int id)
        {
            var result = _ChildTestPerformanceRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditChildTestPerformance")]
        [HttpPost]
        public IActionResult EditChildTestPerformance([FromBody] DTOChildTestPerformance Item)
        {
            var _Item = _ChildTestPerformanceRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddChildTestPerformance")]
        [HttpPost]
        public IActionResult AddChildTestPerformance([FromBody] DTOChildTestPerformance Item)
        {
            var _Item = _ChildTestPerformanceRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteChildTestPerformancesByList")]
        [HttpPost]
        public IActionResult DeleteChildTestPerformancesByList([FromBody] List<DTOChildTestPerformance> Items)
        {
            _ChildTestPerformanceRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteChildTestPerformanceByItem")]
        [HttpPost]
        public IActionResult DeleteChildTestPerformanceByItem([FromBody] DTOChildTestPerformance Item)
        {
            _ChildTestPerformanceRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteChildTestPerformancesByIds")]
        [HttpPost]
        public IActionResult DeleteChildTestPerformancesByIds([FromBody] List<int> ItemIds)
        {
            _ChildTestPerformanceRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteChildTestPerformanceById")]
        [HttpPost]
        public IActionResult DeleteChildTestPerformanceById([FromBody] int ItemId)
        {
            _ChildTestPerformanceRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
