using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class PlanController : Controller
    {
        private readonly IPlanRepository _PlanRepository;
        public PlanController(IPlanRepository _Plan)
        {
            _PlanRepository = _Plan;
        }

        [Route("~/api/GetAllPlans")]
        [HttpGet]
        public IActionResult GetAllPlans()
        {
            var result = _PlanRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetPlanById")]
        [HttpGet]
        public IActionResult GetPlanById(int id)
        {
            var result = _PlanRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditPlan")]
        [HttpPost]
        public IActionResult EditPlan([FromBody] DTOPlan Item)
        {
            var _Item = _PlanRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddPlan")]
        [HttpPost]
        public IActionResult AddPlan([FromBody] DTOPlan Item)
        {
            var _Item = _PlanRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeletePlansByList")]
        [HttpPost]
        public IActionResult DeletePlansByList([FromBody] List<DTOPlan> Items)
        {
            _PlanRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeletePlanByItem")]
        [HttpPost]
        public IActionResult DeletePlanByItem([FromBody] DTOPlan Item)
        {
            _PlanRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeletePlansByIds")]
        [HttpPost]
        public IActionResult DeletePlansByIds([FromBody] List<int> ItemIds)
        {
            _PlanRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeletePlanById")]
        [HttpPost]
        public IActionResult DeletePlanById([FromBody] int ItemId)
        {
            _PlanRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
