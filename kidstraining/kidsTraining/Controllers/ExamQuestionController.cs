using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{
   
    public class ExamQuestionController : Controller
    {
        private readonly IExamQuestionRepository _ExamQuestionRepository;
        public ExamQuestionController(IExamQuestionRepository _ExamQuestion)
        {
            _ExamQuestionRepository = _ExamQuestion;
        }

        [Route("~/api/GetAllExamQuestions")]
        [HttpGet]
        public IActionResult GetAllExamQuestions()
        {
            var result = _ExamQuestionRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetExamQuestionById")]
        [HttpGet]
        public IActionResult GetExamQuestionById(int id)
        {
            var result = _ExamQuestionRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditExamQuestion")]
        [HttpPost]
        public IActionResult EditExamQuestion([FromBody] DTOExamQuestion Item)
        {
            var _Item = _ExamQuestionRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddExamQuestion")]
        [HttpPost]
        public IActionResult AddExamQuestion([FromBody] DTOExamQuestion Item)
        {
            var _Item = _ExamQuestionRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteExamQuestionsByList")]
        [HttpPost]
        public IActionResult DeleteExamQuestionsByList([FromBody] List<DTOExamQuestion> Items)
        {
            _ExamQuestionRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteExamQuestionByItem")]
        [HttpPost]
        public IActionResult DeleteExamQuestionByItem([FromBody] DTOExamQuestion Item)
        {
            _ExamQuestionRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteExamQuestionsByIds")]
        [HttpPost]
        public IActionResult DeleteExamQuestionsByIds([FromBody] List<int> ItemIds)
        {
            _ExamQuestionRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteExamQuestionById")]
        [HttpPost]
        public IActionResult DeleteExamQuestionById([FromBody] int ItemId)
        {
            _ExamQuestionRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
