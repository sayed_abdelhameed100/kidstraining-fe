using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class ChildExamController : Controller
    {
        private readonly IChildExamRepository _ChildExamRepository;
        public ChildExamController(IChildExamRepository _ChildExam)
        {
            _ChildExamRepository = _ChildExam;
        }

        [Route("~/api/GetAllChildExams")]
        [HttpGet]
        public IActionResult GetAllChildExams()
        {
            var result = _ChildExamRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetChildExamById")]
        [HttpGet]
        public IActionResult GetChildExamById(int id)
        {
            var result = _ChildExamRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditChildExam")]
        [HttpPost]
        public IActionResult EditChildExam([FromBody] DTOChildExam Item)
        {
            var _Item = _ChildExamRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddChildExam")]
        [HttpPost]
        public IActionResult AddChildExam([FromBody] DTOChildExam Item)
        {
            var _Item = _ChildExamRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteChildExamsByList")]
        [HttpPost]
        public IActionResult DeleteChildExamsByList([FromBody] List<DTOChildExam> Items)
        {
            _ChildExamRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteChildExamByItem")]
        [HttpPost]
        public IActionResult DeleteChildExamByItem([FromBody] DTOChildExam Item)
        {
            _ChildExamRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteChildExamsByIds")]
        [HttpPost]
        public IActionResult DeleteChildExamsByIds([FromBody] List<int> ItemIds)
        {
            _ChildExamRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteChildExamById")]
        [HttpPost]
        public IActionResult DeleteChildExamById([FromBody] int ItemId)
        {
            _ChildExamRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
