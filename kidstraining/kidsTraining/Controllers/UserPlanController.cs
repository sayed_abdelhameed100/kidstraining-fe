using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Authorization;

namespace kidsTraining.Controllers
{
 
    public class UserPlanController : Controller
    {
        private readonly IUserPlanRepository _UserPlanRepository;
        public UserPlanController(IUserPlanRepository _UserPlan)
        {
            _UserPlanRepository = _UserPlan;
        }

        [Route("~/api/GetAllUserPlans")]
        [HttpGet]
        public IActionResult GetAllUserPlans()
        {
            var result = _UserPlanRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetUserPlanById")]
        [HttpGet]
        public IActionResult GetUserPlanById(int id)
        {
            var result = _UserPlanRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditUserPlan")]
        [HttpPost]
        public IActionResult EditUserPlan([FromBody] DTOUserPlan Item)
        {
            var _Item = _UserPlanRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddUserPlan")]
        [HttpPost]
        [Authorize]
        public IActionResult AddUserPlan([FromBody] DTOUserPlan Item)
        {
            var _Item = _UserPlanRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteUserPlansByList")]
        [HttpPost]
        public IActionResult DeleteUserPlansByList([FromBody] List<DTOUserPlan> Items)
        {
            _UserPlanRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteUserPlanByItem")]
        [HttpPost]
        public IActionResult DeleteUserPlanByItem([FromBody] DTOUserPlan Item)
        {
            _UserPlanRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteUserPlansByIds")]
        [HttpPost]
        public IActionResult DeleteUserPlansByIds([FromBody] List<int> ItemIds)
        {
            _UserPlanRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteUserPlanById")]
        [HttpPost]
        public IActionResult DeleteUserPlanById([FromBody] int ItemId)
        {
            _UserPlanRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
