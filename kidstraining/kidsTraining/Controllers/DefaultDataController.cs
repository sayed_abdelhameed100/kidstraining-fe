using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class DefaultDataController : Controller
    {
        private readonly IDefaultDataRepository _DefaultDataRepository;
        public DefaultDataController(IDefaultDataRepository _DefaultData)
        {
            _DefaultDataRepository = _DefaultData;
        }

        [Route("~/api/GetAllDefaultDatasByType")]
        [HttpGet]
        public IActionResult GetAllDefaultDatasByType(int? TypeId )
        {
            var result = _DefaultDataRepository.SelectAll(TypeId);
            return Ok(result);
        }

        [Route("~/api/GetAllDefaultDataForDropDown")]
        [HttpGet]
        public IActionResult GetAllDefaultDataForDropDown(int? TypeId)
        {
            var result = _DefaultDataRepository.SelectAllForDropDown(TypeId);
            return Ok(result);
        }

        [Route("~/api/GetDefaultDataById")]
        [HttpGet]
        public IActionResult GetDefaultDataById(int id)
        {
            var result = _DefaultDataRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditDefaultData")]
        [HttpPost]
        public IActionResult EditDefaultData([FromBody] DTODefaultData Item)
        {
            var _Item = _DefaultDataRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddDefaultData")]
        [HttpPost]
        public IActionResult AddDefaultData([FromBody] DTODefaultData Item)
        {
            var _Item = _DefaultDataRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteDefaultDatasByList")]
        [HttpPost]
        public IActionResult DeleteDefaultDatasByList([FromBody] List<DTODefaultData> Items)
        {
            _DefaultDataRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteDefaultDataByItem")]
        [HttpPost]
        public IActionResult DeleteDefaultDataByItem([FromBody] DTODefaultData Item)
        {
            _DefaultDataRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteDefaultDatasByIds")]
        [HttpPost]
        public IActionResult DeleteDefaultDatasByIds([FromBody] List<int> ItemIds)
        {
            _DefaultDataRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteDefaultDataById")]
        [HttpPost]
        public IActionResult DeleteDefaultDataById([FromBody] int ItemId)
        {
            _DefaultDataRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
