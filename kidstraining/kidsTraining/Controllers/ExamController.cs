using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class ExamController : Controller
    {
        private readonly IExamRepository _ExamRepository;
        public ExamController(IExamRepository _Exam)
        {
            _ExamRepository = _Exam;
        }

        [Route("~/api/GetAllExams")]
        [HttpGet]
        public IActionResult GetAllExams()
        {
            var result = _ExamRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetExamById")]
        [HttpGet]
        public IActionResult GetExamById(int id)
        {
            var result = _ExamRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditExam")]
        [HttpPost]
        public IActionResult EditExam([FromBody] DTOExam Item)
        {
            var _Item = _ExamRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddExam")]
        [HttpPost]
        public IActionResult AddExam([FromBody] DTOExam Item)
        {
            var _Item = _ExamRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteExamsByList")]
        [HttpPost]
        public IActionResult DeleteExamsByList([FromBody] List<DTOExam> Items)
        {
            _ExamRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteExamByItem")]
        [HttpPost]
        public IActionResult DeleteExamByItem([FromBody] DTOExam Item)
        {
            _ExamRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteExamsByIds")]
        [HttpPost]
        public IActionResult DeleteExamsByIds([FromBody] List<int> ItemIds)
        {
            _ExamRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteExamById")]
        [HttpPost]
        public IActionResult DeleteExamById([FromBody] int ItemId)
        {
            _ExamRepository.DeleteItems(ItemId);
            return Ok();
        }

        [Route("~/api/SelectExamsByAge")]
        [HttpGet]
        public IActionResult SelectExamsByAge(int Age)
        {
            var result = _ExamRepository.SelectExamsByAge(Age);
            return Ok(result);
        }

        [Route("~/api/FirstChildTest")]
        [HttpGet]
        public IActionResult FirstChildTest(int Age)
        {
            var result = _ExamRepository.FirstChildTest(Age);
            return Ok(result);
        }

        [Route("~/api/SelectChildPerformanceBaseExamsByAge")]
        [HttpGet]
        public IActionResult SelectChildPerformanceBaseExamsByAge(int Age)
        {
            var result = _ExamRepository.SelectChildPerformanceBaseExamsByAge(Age);
            return Ok(result);
        }

        [Route("~/api/SelectChildPerformanceCeilingExamsByAge")]
        [HttpGet]
        public IActionResult SelectChildPerformanceCeilingExamsByAge(int Age)
        {
            var result = _ExamRepository.SelectChildPerformanceCeilingExamsByAge(Age);
            return Ok(result);
        }
    }
}
