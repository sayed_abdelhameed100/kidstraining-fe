using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_DataService.Repository;
using kidsTraining_Interface.IDataService;
using kidsTraining_Model.DTOModel;
using Microsoft.AspNetCore.Authorization;

namespace kidsTraining.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserRepository _repo;
        public UserController(IUserRepository repo)
        {
            _repo = repo;
        }

        [Route("~/api/DeleteUser")]
        [HttpPost]
        [Authorize]
        public IActionResult DeleteUser([FromBody] DTOUser obj)
        {
            _repo.DeleteItems(obj);
            return Ok();
        }

        [Route("~/api/AddUser")]
        [HttpPost]
        public IActionResult AddUser([FromBody] DTOUser obj)
        {
            return Ok(_repo.AddItem(obj));
        }

        [Route("~/api/EditUser")]
        [HttpPost]
        public IActionResult EditUser([FromBody] DTOUser obj)
        {
            return Ok(_repo.EditItem(obj));
        }

        [Route("~/api/GetAllUsers")]
        [HttpGet]
        [Authorize]
        public IActionResult GetAllUsers()
        {
            return Ok(_repo.GetAll());
        }

        [Route("~/api/Login")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult Authenticate([FromBody] DTOUser obj)
        {
            var user = _repo.Authenticate(obj.UserName, obj.Password);

            if (user == null) return Ok("1");

            else
            {
                if (user.IsVerifyOtp != true) return Ok(new { msg = "otp", user = user });
                if (user.IsActice != true) return Ok(new { msg = "notactive", user = user });

                return Ok(user);
            }

        }


        [Route("~/api/CheckUser")]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult CheckUser(string UserName)
        {
            return Ok(_repo.CheckUser(UserName));
        }

        [Route("~/api/CheckUserEmail")]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult CheckUserEmail(string Email)
        {
            return Ok(_repo.CheckUserEmail(Email));
        }

        [Route("~/api/CheckUserNumber")]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult CheckUserNumber(string Phone)
        {
            return Ok(_repo.CheckUserNumber(Phone));
        }


        [Route("~/api/ForGetPasswaord")]
        [AllowAnonymous]
        [HttpPost]
        public IActionResult ForGetPasswaord([FromBody] DTOUser obj)
        {
            return Ok(_repo.ForGetPasswaord(obj.Id, obj.Password));

        }

        [Route("~/api/CheckOtp")]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult CheckOtp(int UserId, int? Otp)
        {
            return Ok(_repo.CheckOtp(UserId, Otp));
        }

        [Route("~/api/ReSendOtp")]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult ReSendOtp(string Phone)
        {
            _repo.SendOtp(Phone);
            return Ok();
        }


        //[Route("~/api/AuthenticateSocial")]
        //[AllowAnonymous]
        //[HttpPost]
        //public IActionResult AuthenticateSocial(string Name, string Email, long? ProvideruserId)
        //{
        //    var user = _repo.AuthenticateSocial(Name, Email, ProvideruserId);
        //    string msg = "";
        //    if (user == null)
        //    {
        //        msg = "1";
        //        return Ok(msg);
        //    }

        //    else
        //    {
        //        return Ok(user);
        //    }

        //}

        [Route("~/api/GetUserInfo")]
        [HttpGet]
        [Authorize]
        public IActionResult GetUserInfo()
        {
            return Ok(_repo.GetCurrentUserInfo());
        }

        [Route("~/api/GetUserById")]
        [HttpGet]
        [Authorize]
        public IActionResult GetUserById(int Id)
        {
            return Ok(_repo.SelectById(Id));
        }

        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string Base64Decode(string base64EncodedData)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }
    }
}
