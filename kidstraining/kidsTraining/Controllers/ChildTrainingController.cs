using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class ChildTrainingController : Controller
    {
        private readonly IChildTrainingRepository _ChildTrainingRepository;
        public ChildTrainingController(IChildTrainingRepository _ChildTraining)
        {
            _ChildTrainingRepository = _ChildTraining;
        }

        [Route("~/api/GetAllChildTrainings")]
        [HttpGet]
        public IActionResult GetAllChildTrainings()
        {
            var result = _ChildTrainingRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetChildTrainingById")]
        [HttpGet]
        public IActionResult GetChildTrainingById(int id)
        {
            var result = _ChildTrainingRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditChildTraining")]
        [HttpPost]
        public IActionResult EditChildTraining([FromBody] DTOChildTraining Item)
        {
            var _Item = _ChildTrainingRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddChildTraining")]
        [HttpPost]
        public IActionResult AddChildTraining([FromBody] DTOChildTraining Item)
        {
            var _Item = _ChildTrainingRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteChildTrainingsByList")]
        [HttpPost]
        public IActionResult DeleteChildTrainingsByList([FromBody] List<DTOChildTraining> Items)
        {
            _ChildTrainingRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteChildTrainingByItem")]
        [HttpPost]
        public IActionResult DeleteChildTrainingByItem([FromBody] DTOChildTraining Item)
        {
            _ChildTrainingRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteChildTrainingsByIds")]
        [HttpPost]
        public IActionResult DeleteChildTrainingsByIds([FromBody] List<int> ItemIds)
        {
            _ChildTrainingRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteChildTrainingById")]
        [HttpPost]
        public IActionResult DeleteChildTrainingById([FromBody] int ItemId)
        {
            _ChildTrainingRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
