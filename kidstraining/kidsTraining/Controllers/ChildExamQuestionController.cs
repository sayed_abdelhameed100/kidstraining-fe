using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using kidsTraining_Model.DTOModel;
using kidsTraining_Interface.IDataService;
using Microsoft.AspNetCore.Cors;

namespace kidsTraining.Controllers
{

    public class ChildExamQuestionController : Controller
    {
        private readonly IChildExamQuestionRepository _ChildExamQuestionRepository;
        public ChildExamQuestionController(IChildExamQuestionRepository _ChildExamQuestion)
        {
            _ChildExamQuestionRepository = _ChildExamQuestion;
        }

        [Route("~/api/GetAllChildExamQuestions")]
        [HttpGet]
        public IActionResult GetAllChildExamQuestions()
        {
            var result = _ChildExamQuestionRepository.SelectAll();
            return Ok(result);
        }

        [Route("~/api/GetChildExamQuestionById")]
        [HttpGet]
        public IActionResult GetChildExamQuestionById(int id)
        {
            var result = _ChildExamQuestionRepository.SelectById(id);
            return Ok(result);
        }

        [Route("~/api/EditChildExamQuestion")]
        [HttpPost]
        public IActionResult EditChildExamQuestion([FromBody] DTOChildExamQuestion Item)
        {
            var _Item = _ChildExamQuestionRepository.EditItem(Item);
            return Ok();
        }

        [Route("~/api/AddChildExamQuestion")]
        [HttpPost]
        public IActionResult AddChildExamQuestion([FromBody] DTOChildExamQuestion Item)
        {
            var _Item = _ChildExamQuestionRepository.AddItem(Item);

            return Ok(_Item);
        }

        [Route("~/api/DeleteChildExamQuestionsByList")]
        [HttpPost]
        public IActionResult DeleteChildExamQuestionsByList([FromBody] List<DTOChildExamQuestion> Items)
        {
            _ChildExamQuestionRepository.DeleteItems(Items);
            return Ok();
        }

        [Route("~/api/DeleteChildExamQuestionByItem")]
        [HttpPost]
        public IActionResult DeleteChildExamQuestionByItem([FromBody] DTOChildExamQuestion Item)
        {
            _ChildExamQuestionRepository.DeleteItems(Item);
            return Ok();
        }

        [Route("~/api/DeleteChildExamQuestionsByIds")]
        [HttpPost]
        public IActionResult DeleteChildExamQuestionsByIds([FromBody] List<int> ItemIds)
        {
            _ChildExamQuestionRepository.DeleteItems(ItemIds);
            return Ok();
        }

        [Route("~/api/DeleteChildExamQuestionById")]
        [HttpPost]
        public IActionResult DeleteChildExamQuestionById([FromBody] int ItemId)
        {
            _ChildExamQuestionRepository.DeleteItems(ItemId);
            return Ok();
        }


    }
}
