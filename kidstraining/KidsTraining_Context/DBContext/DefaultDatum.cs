﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class DefaultDatum
    {
        public DefaultDatum()
        {
            Children = new HashSet<Child>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public int? Type { get; set; }

        public virtual ICollection<Child> Children { get; set; }
    }
}
