﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class User
    {
        public User()
        {
            Children = new HashSet<Child>();
            UserPlans = new HashSet<UserPlan>();
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public DateTime? CreateOn { get; set; }
        public bool? IsActice { get; set; }
        public string LogoUrl { get; set; }
        public int? OtpCode { get; set; }
        public DateTime? OtpDate { get; set; }
        public bool? IsVerifyOtp { get; set; }

        public virtual ICollection<Child> Children { get; set; }
        public virtual ICollection<UserPlan> UserPlans { get; set; }
    }
}
