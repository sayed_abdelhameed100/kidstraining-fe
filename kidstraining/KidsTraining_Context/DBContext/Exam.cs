﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class Exam
    {
        public Exam()
        {
            ChildExams = new HashSet<ChildExam>();
            ChildTrainings = new HashSet<ChildTraining>();
            ExamQuestions = new HashSet<ExamQuestion>();
            InverseParentt = new HashSet<Exam>();
        }

        public int Id { get; set; }
        public int? ExamCode { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public int? ExamAge { get; set; }
        public int? Degree { get; set; }
        public int? ParenttId { get; set; }
        public int? SortNumber { get; set; }

        public virtual Exam Parentt { get; set; }
        public virtual ICollection<ChildExam> ChildExams { get; set; }
        public virtual ICollection<ChildTraining> ChildTrainings { get; set; }
        public virtual ICollection<ExamQuestion> ExamQuestions { get; set; }
        public virtual ICollection<Exam> InverseParentt { get; set; }
    }
}
