﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class kidsTrainingDBContext : DbContext
    {
        public kidsTrainingDBContext()
        {
        }

        public kidsTrainingDBContext(DbContextOptions<kidsTrainingDBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Child> Children { get; set; }
        public virtual DbSet<ChildExam> ChildExams { get; set; }
        public virtual DbSet<ChildExamQuestion> ChildExamQuestions { get; set; }
        public virtual DbSet<ChildTestPerformance> ChildTestPerformances { get; set; }
        public virtual DbSet<ChildTraining> ChildTrainings { get; set; }
        public virtual DbSet<ChildTrainingQuestion> ChildTrainingQuestions { get; set; }
        public virtual DbSet<DefaultDatum> DefaultData { get; set; }
        public virtual DbSet<Exam> Exams { get; set; }
        public virtual DbSet<ExamQuestion> ExamQuestions { get; set; }
        public virtual DbSet<Plan> Plans { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserPlan> UserPlans { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseSqlServer("server=DESKTOP-1J5LQOA\\SQLEXPRESS;database=kidsTrainingDB;integrated security=true");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AS");

            modelBuilder.Entity<Child>(entity =>
            {
                entity.ToTable("children");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AgeInMonth).HasColumnName("ageInMonth");

                entity.Property(e => e.Birthdate)
                    .HasColumnType("datetime")
                    .HasColumnName("birthdate");

                entity.Property(e => e.ChildLang)
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .HasColumnName("childLang");

                entity.Property(e => e.Diagnosis).HasColumnName("diagnosis");

                entity.Property(e => e.KeyWordLengthId).HasColumnName("keyWordLengthId");

                entity.Property(e => e.Name)
                    .HasMaxLength(200)
                    .HasColumnName("name");

                entity.Property(e => e.Sex).HasColumnName("sex");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.KeyWordLength)
                    .WithMany(p => p.Children)
                    .HasForeignKey(d => d.KeyWordLengthId)
                    .HasConstraintName("FK_children_defaultData");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Children)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_children_user");
            });

            modelBuilder.Entity<ChildExam>(entity =>
            {
                entity.ToTable("childExam");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChildId).HasColumnName("childId");

                entity.Property(e => e.ChildTestPerformanceId).HasColumnName("childTestPerformanceId");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("creationDate");

                entity.Property(e => e.ExamId).HasColumnName("examId");

                entity.Property(e => e.TotalDegree).HasColumnName("totalDegree");

                entity.HasOne(d => d.Child)
                    .WithMany(p => p.ChildExams)
                    .HasForeignKey(d => d.ChildId)
                    .HasConstraintName("FK_childExam_children");

                entity.HasOne(d => d.Exam)
                    .WithMany(p => p.ChildExams)
                    .HasForeignKey(d => d.ExamId)
                    .HasConstraintName("FK_childExam_exam");
            });

            modelBuilder.Entity<ChildExamQuestion>(entity =>
            {
                entity.ToTable("childExamQuestion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChildExamId).HasColumnName("childExamId");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("creationDate");

                entity.Property(e => e.Degree).HasColumnName("degree");

                entity.Property(e => e.ExamQuestionId).HasColumnName("examQuestionId");

                entity.HasOne(d => d.ChildExam)
                    .WithMany(p => p.ChildExamQuestions)
                    .HasForeignKey(d => d.ChildExamId)
                    .HasConstraintName("FK_childExamQuestion_childExam");

                entity.HasOne(d => d.ExamQuestion)
                    .WithMany(p => p.ChildExamQuestions)
                    .HasForeignKey(d => d.ExamQuestionId)
                    .HasConstraintName("FK_childExamQuestion_examQuestion");
            });

            modelBuilder.Entity<ChildTestPerformance>(entity =>
            {
                entity.ToTable("childTestPerformance");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChildId).HasColumnName("childId");

                entity.Property(e => e.ChildPerformanceBaseId).HasColumnName("childPerformanceBaseId");

                entity.Property(e => e.ChildPerformanceCeilingId).HasColumnName("childPerformanceCeilingId");

                entity.Property(e => e.CreationDate)
                    .HasColumnType("smalldatetime")
                    .HasColumnName("creationDate");

                entity.Property(e => e.ExamStartId).HasColumnName("examStartId");

                entity.Property(e => e.IsCurrentTest)
                    .HasColumnName("isCurrentTest")
                    .HasDefaultValueSql("((0))");

                entity.HasOne(d => d.Child)
                    .WithMany(p => p.ChildTestPerformances)
                    .HasForeignKey(d => d.ChildId)
                    .HasConstraintName("FK_childTestPerformance_children");

                entity.HasOne(d => d.ChildPerformanceCeiling)
                    .WithMany(p => p.ChildTestPerformances)
                    .HasForeignKey(d => d.ChildPerformanceCeilingId)
                    .HasConstraintName("FK_childTestPerformance_childExam");
            });

            modelBuilder.Entity<ChildTraining>(entity =>
            {
                entity.ToTable("childTraining");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChildId).HasColumnName("childId");

                entity.Property(e => e.ExamId).HasColumnName("examId");

                entity.Property(e => e.FinishedTraining).HasColumnName("finishedTraining");

                entity.HasOne(d => d.Child)
                    .WithMany(p => p.ChildTrainings)
                    .HasForeignKey(d => d.ChildId)
                    .HasConstraintName("FK_childTraining_children");

                entity.HasOne(d => d.Exam)
                    .WithMany(p => p.ChildTrainings)
                    .HasForeignKey(d => d.ExamId)
                    .HasConstraintName("FK_childTraining_exam");
            });

            modelBuilder.Entity<ChildTrainingQuestion>(entity =>
            {
                entity.ToTable("childTrainingQuestion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ChildTrainingId).HasColumnName("childTrainingId");

                entity.Property(e => e.ExamQuestionId).HasColumnName("examQuestionId");

                entity.Property(e => e.FinishedTraining).HasColumnName("finishedTraining");

                entity.HasOne(d => d.ChildTraining)
                    .WithMany(p => p.ChildTrainingQuestions)
                    .HasForeignKey(d => d.ChildTrainingId)
                    .HasConstraintName("FK_childTrainingQuestion_childTraining");

                entity.HasOne(d => d.ExamQuestion)
                    .WithMany(p => p.ChildTrainingQuestions)
                    .HasForeignKey(d => d.ExamQuestionId)
                    .HasConstraintName("FK_childTrainingQuestion_examQuestion");
            });

            modelBuilder.Entity<DefaultDatum>(entity =>
            {
                entity.ToTable("defaultData");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.NameAr).HasColumnName("nameAr");

                entity.Property(e => e.NameEn).HasColumnName("nameEn");

                entity.Property(e => e.Type).HasColumnName("type");
            });

            modelBuilder.Entity<Exam>(entity =>
            {
                entity.ToTable("exam");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Degree).HasColumnName("degree");

                entity.Property(e => e.ExamAge).HasColumnName("examAge");

                entity.Property(e => e.ExamCode).HasColumnName("examCode");

                entity.Property(e => e.NameAr).HasColumnName("nameAr");

                entity.Property(e => e.NameEn).HasColumnName("nameEn");

                entity.Property(e => e.ParenttId).HasColumnName("parenttId");

                entity.Property(e => e.SortNumber).HasColumnName("sortNumber");

                entity.HasOne(d => d.Parentt)
                    .WithMany(p => p.InverseParentt)
                    .HasForeignKey(d => d.ParenttId)
                    .HasConstraintName("FK_exam_exam");
            });

            modelBuilder.Entity<ExamQuestion>(entity =>
            {
                entity.ToTable("examQuestion");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DescriptionAr).HasColumnName("descriptionAr");

                entity.Property(e => e.DescriptionEn).HasColumnName("descriptionEn");

                entity.Property(e => e.ExamId).HasColumnName("examId");

                entity.Property(e => e.ExamQuestionCode).HasColumnName("examQuestionCode");

                entity.Property(e => e.NameAr).HasColumnName("nameAr");

                entity.Property(e => e.NameEn).HasColumnName("nameEn");

                entity.Property(e => e.SortNumber).HasColumnName("sortNumber");

                entity.Property(e => e.VideoUrl).HasColumnName("videoUrl");

                entity.HasOne(d => d.Exam)
                    .WithMany(p => p.ExamQuestions)
                    .HasForeignKey(d => d.ExamId)
                    .HasConstraintName("FK_examQuestion_exam");
            });

            modelBuilder.Entity<Plan>(entity =>
            {
                entity.ToTable("plan");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Cost).HasColumnName("cost");

                entity.Property(e => e.IsPay)
                    .HasColumnName("isPay")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.NameAr).HasColumnName("nameAr");

                entity.Property(e => e.NameEn).HasColumnName("nameEn");

                entity.Property(e => e.TimePerMonth).HasColumnName("timePerMonth");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("user");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address).HasColumnName("address");

                entity.Property(e => e.CreateOn)
                    .HasColumnType("datetime")
                    .HasColumnName("createOn");

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("email");

                entity.Property(e => e.IsActice)
                    .HasColumnName("isActice")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsVerifyOtp)
                    .HasColumnName("isVerifyOtp")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.LogoUrl).HasColumnName("logoUrl");

                entity.Property(e => e.NameAr)
                    .HasMaxLength(150)
                    .HasColumnName("nameAR");

                entity.Property(e => e.NameEn)
                    .HasMaxLength(150)
                    .HasColumnName("nameEn");

                entity.Property(e => e.OtpCode).HasColumnName("otpCode");

                entity.Property(e => e.OtpDate)
                    .HasColumnType("datetime")
                    .HasColumnName("otpDate");

                entity.Property(e => e.Password).HasColumnName("password");

                entity.Property(e => e.Phone)
                    .HasMaxLength(15)
                    .IsUnicode(false)
                    .HasColumnName("phone");

                entity.Property(e => e.UserName)
                    .HasMaxLength(150)
                    .HasColumnName("userName");
            });

            modelBuilder.Entity<UserPlan>(entity =>
            {
                entity.ToTable("userPlan");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.EndDate)
                    .HasColumnType("datetime")
                    .HasColumnName("endDate");

                entity.Property(e => e.IsCurrentPlan).HasColumnName("isCurrentPlan");

                entity.Property(e => e.PaymentDate)
                    .HasColumnType("datetime")
                    .HasColumnName("paymentDate");

                entity.Property(e => e.PaymentRefNum).HasColumnName("paymentRefNum");

                entity.Property(e => e.PaymentStatus)
                    .HasColumnName("paymentStatus")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.PlanId).HasColumnName("planId");

                entity.Property(e => e.StartDate)
                    .HasColumnType("datetime")
                    .HasColumnName("startDate");

                entity.Property(e => e.Total).HasColumnName("total");

                entity.Property(e => e.UserId).HasColumnName("userId");

                entity.HasOne(d => d.Plan)
                    .WithMany(p => p.UserPlans)
                    .HasForeignKey(d => d.PlanId)
                    .HasConstraintName("FK_userPlan_plan");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserPlans)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_userPlan_user");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
