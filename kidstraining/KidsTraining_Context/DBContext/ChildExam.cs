﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class ChildExam
    {
        public ChildExam()
        {
            ChildExamQuestions = new HashSet<ChildExamQuestion>();
            ChildTestPerformances = new HashSet<ChildTestPerformance>();
        }

        public int Id { get; set; }
        public int? ChildId { get; set; }
        public int? ExamId { get; set; }
        public int? TotalDegree { get; set; }
        public int? ChildTestPerformanceId { get; set; }
        public DateTime? CreationDate { get; set; }

        public virtual Child Child { get; set; }
        public virtual Exam Exam { get; set; }
        public virtual ICollection<ChildExamQuestion> ChildExamQuestions { get; set; }
        public virtual ICollection<ChildTestPerformance> ChildTestPerformances { get; set; }
    }
}
