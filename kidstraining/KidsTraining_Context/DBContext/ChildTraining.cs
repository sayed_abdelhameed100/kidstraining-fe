﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class ChildTraining
    {
        public ChildTraining()
        {
            ChildTrainingQuestions = new HashSet<ChildTrainingQuestion>();
        }

        public int Id { get; set; }
        public int? ChildId { get; set; }
        public int? ExamId { get; set; }
        public bool? FinishedTraining { get; set; }

        public virtual Child Child { get; set; }
        public virtual Exam Exam { get; set; }
        public virtual ICollection<ChildTrainingQuestion> ChildTrainingQuestions { get; set; }
    }
}
