﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class ChildTestPerformance
    {
        public int Id { get; set; }
        public int? ChildId { get; set; }
        public DateTime? CreationDate { get; set; }
        public int? ExamStartId { get; set; }
        public bool? IsCurrentTest { get; set; }
        public int? ChildPerformanceCeilingId { get; set; }
        public int? ChildPerformanceBaseId { get; set; }

        public virtual Child Child { get; set; }
        public virtual ChildExam ChildPerformanceCeiling { get; set; }
    }
}
