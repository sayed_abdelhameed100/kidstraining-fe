﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class ChildTrainingQuestion
    {
        public int Id { get; set; }
        public int? ChildTrainingId { get; set; }
        public bool? FinishedTraining { get; set; }
        public int? ExamQuestionId { get; set; }

        public virtual ChildTraining ChildTraining { get; set; }
        public virtual ExamQuestion ExamQuestion { get; set; }
    }
}
