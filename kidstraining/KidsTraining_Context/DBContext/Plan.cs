﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class Plan
    {
        public Plan()
        {
            UserPlans = new HashSet<UserPlan>();
        }

        public int Id { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public double? Cost { get; set; }
        public bool? IsPay { get; set; }
        public int? TimePerMonth { get; set; }

        public virtual ICollection<UserPlan> UserPlans { get; set; }
    }
}
