﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class UserPlan
    {
        public int Id { get; set; }
        public int? UserId { get; set; }
        public int? PlanId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool? IsCurrentPlan { get; set; }
        public bool? PaymentStatus { get; set; }
        public long? PaymentRefNum { get; set; }
        public DateTime? PaymentDate { get; set; }
        public double? Total { get; set; }

        public virtual Plan Plan { get; set; }
        public virtual User User { get; set; }
    }
}
