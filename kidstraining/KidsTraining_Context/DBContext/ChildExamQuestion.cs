﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class ChildExamQuestion
    {
        public int Id { get; set; }
        public int? ChildExamId { get; set; }
        public int? ExamQuestionId { get; set; }
        public int? Degree { get; set; }
        public DateTime? CreationDate { get; set; }

        public virtual ChildExam ChildExam { get; set; }
        public virtual ExamQuestion ExamQuestion { get; set; }
    }
}
