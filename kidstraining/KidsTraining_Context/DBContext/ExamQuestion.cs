﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class ExamQuestion
    {
        public ExamQuestion()
        {
            ChildExamQuestions = new HashSet<ChildExamQuestion>();
            ChildTrainingQuestions = new HashSet<ChildTrainingQuestion>();
        }

        public int Id { get; set; }
        public int? ExamQuestionCode { get; set; }
        public string NameEn { get; set; }
        public string NameAr { get; set; }
        public int? ExamId { get; set; }
        public int? SortNumber { get; set; }
        public string VideoUrl { get; set; }
        public string DescriptionAr { get; set; }
        public string DescriptionEn { get; set; }

        public virtual Exam Exam { get; set; }
        public virtual ICollection<ChildExamQuestion> ChildExamQuestions { get; set; }
        public virtual ICollection<ChildTrainingQuestion> ChildTrainingQuestions { get; set; }
    }
}
