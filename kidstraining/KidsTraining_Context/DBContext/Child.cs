﻿using System;
using System.Collections.Generic;

#nullable disable

namespace KidsTraining_Context.DBContext
{
    public partial class Child
    {
        public Child()
        {
            ChildExams = new HashSet<ChildExam>();
            ChildTestPerformances = new HashSet<ChildTestPerformance>();
            ChildTrainings = new HashSet<ChildTraining>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string ChildLang { get; set; }
        public int? Sex { get; set; }
        public string Diagnosis { get; set; }
        public DateTime? Birthdate { get; set; }
        public int? AgeInMonth { get; set; }
        public int? UserId { get; set; }
        public int? KeyWordLengthId { get; set; }

        public virtual DefaultDatum KeyWordLength { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<ChildExam> ChildExams { get; set; }
        public virtual ICollection<ChildTestPerformance> ChildTestPerformances { get; set; }
        public virtual ICollection<ChildTraining> ChildTrainings { get; set; }
    }
}
